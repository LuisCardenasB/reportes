-- Copyright (C) ---Put here your own copyright and developer email---
--
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
--
-- You should have received a copy of the GNU General Public License
-- along with this program.  If not, see <http://www.gnu.org/licenses/>.

INSERT INTO llx_reports_catalog (label, description, status, technical_name, date_creation, tms) 
SELECT * FROM(
	SELECT 'Auxiliar Contable' as label, 'Reporte para visualizar el Auxiliar Contable' as descr, 0 as status, 'auxiliar' as techname, now() as created_at, now() as tms
) as T WHERE NOT EXISTS (
    SELECT label FROM llx_reports_catalog WHERE label = 'Auxiliar Contable'
);

INSERT INTO llx_reports_catalog (label, description, status, technical_name, date_creation, tms) 
SELECT * FROM(
	SELECT 'Balance General' as label, 'Reporte para visualizar el Balance General' as descr, 0 as status, 'general_bal' as techname, now() as created_at, now() as tms
) as T WHERE NOT EXISTS (
    SELECT label FROM llx_reports_catalog WHERE label = 'Balance General'
);

INSERT INTO llx_reports_catalog (label, description, status, technical_name, date_creation, tms) 
SELECT * FROM(
	SELECT 'Balanza de Comprobación' as label, 'Reporte para visualizar el Balanza de Comprobación' as descr, 0 as status, 'comp_bal' as techname, now() as created_at, now() as tms
) as T WHERE NOT EXISTS (
    SELECT label FROM llx_reports_catalog WHERE label = 'Balanza de Comprobación'
);

INSERT INTO llx_reports_catalog (label, description, status, technical_name, date_creation, tms) 
SELECT * FROM(
	SELECT 'Estado de Resultados' as label, 'Reporte para visualizar el Estado de Resultados' as descr, 0 as status, 'results_edo' as techname, now() as created_at, now() as tms
) as T WHERE NOT EXISTS (
    SELECT label FROM llx_reports_catalog WHERE label = 'Estado de Resultados'
);

INSERT INTO llx_reports_catalog (label, description, status, technical_name, date_creation, tms) 
SELECT * FROM(
	SELECT 'Libro Mayor' as label, 'Reporte para visualizar el Libro Mayor' as descr, 0 as status, 'may_book' as techname, now() as created_at, now() as tms
) as T WHERE NOT EXISTS (
    SELECT label FROM llx_reports_catalog WHERE label = 'Libro Mayor'
);

INSERT INTO llx_reports_catalog (label, description, status, technical_name, date_creation, tms) 
SELECT * FROM(
	SELECT 'Reporte DIOT' as label, 'Reporte para visualizar la DIOT' as descr, 0 as status, 'diot' as techname, now() as created_at, now() as tms
) as T WHERE NOT EXISTS (
    SELECT label FROM llx_reports_catalog WHERE label = 'Reporte DIOT'
);

INSERT INTO llx_reports_catalog (label, description, status, technical_name, date_creation, tms) 
SELECT * FROM(
	SELECT 'Estado de Cuentas' as label, 'Reporte para visualizar el Estado de Cuentas' as descr, 0 as status, 'edo_ctas' as techname, now() as created_at, now() as tms
) as T WHERE NOT EXISTS (
    SELECT label FROM llx_reports_catalog WHERE label = 'Estado de Cuentas'
);

INSERT INTO llx_reports_catalog (label, description, status, technical_name, date_creation, tms) 
SELECT * FROM(
	SELECT 'Pólizas' as label, 'Reporte para visualizar las pólizas' as descr, 0 as status, 'polizas' as techname, now() as created_at, now() as tms
) as T WHERE NOT EXISTS (
    SELECT label FROM llx_reports_catalog WHERE label = 'Pólizas'
);

