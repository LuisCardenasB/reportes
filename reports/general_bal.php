<?php
/* Copyright (C) 2004-2017 Laurent Destailleur  <eldy@users.sourceforge.net>
 * Copyright (C) ---Put here your own copyright and developer email---
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/**
 * \file    htdocs/modulebuilder/template/admin/about.php
 * \ingroup mymodule
 * \brief   About page of module MyModule.
 */

// Load Dolibarr environment
$res=0;
// Try main.inc.php into web root known defined into CONTEXT_DOCUMENT_ROOT (not always defined)
if (! $res && ! empty($_SERVER["CONTEXT_DOCUMENT_ROOT"])) $res=@include($_SERVER["CONTEXT_DOCUMENT_ROOT"]."/main.inc.php");
// Try main.inc.php into web root detected using web root caluclated from SCRIPT_FILENAME
$tmp=empty($_SERVER['SCRIPT_FILENAME'])?'':$_SERVER['SCRIPT_FILENAME'];$tmp2=realpath(__FILE__); $i=strlen($tmp)-1; $j=strlen($tmp2)-1;
while($i > 0 && $j > 0 && isset($tmp[$i]) && isset($tmp2[$j]) && $tmp[$i]==$tmp2[$j]) { $i--; $j--; }
if (! $res && $i > 0 && file_exists(substr($tmp, 0, ($i+1))."/main.inc.php")) $res=@include(substr($tmp, 0, ($i+1))."/main.inc.php");
if (! $res && $i > 0 && file_exists(dirname(substr($tmp, 0, ($i+1)))."/main.inc.php")) $res=@include(dirname(substr($tmp, 0, ($i+1)))."/main.inc.php");
// Try main.inc.php using relative path
if (! $res && file_exists("../../main.inc.php")) $res=@include("../../main.inc.php");
if (! $res && file_exists("../../../main.inc.php")) $res=@include("../../../main.inc.php");
if (! $res) die("Include of main fails");


require_once DOL_DOCUMENT_ROOT.'/core/class/html.formfile.class.php';
require_once '../lib/reports.lib.php';


$langs->loadLangs(array("reports@reports"));

$action=GETPOST('action', 'alpha');


// Securite acces client
if (! $user->rights->reports->read) accessforbidden();
$socid=GETPOST('socid','int');
if (isset($user->societe_id) && $user->societe_id > 0)
{
	$action = '';
	$socid = $user->societe_id;
}

$max=5;
$now=dol_now();


/*
 * Actions
 */

// None


/*
 * View
 */

$form = new Form($db);
$formfile = new FormFile($db);

llxHeader("",$langs->trans("Reportes contables"));

print load_fiche_titre($langs->trans("Reportes contables"),'','object_reports.png@reports');

//print '<div class="fichecenter"><div class="fichethirdleft">';

print "Seleccione el reporte que desea consultar.";

$head = reportsUserPrepareHead();
dol_fiche_head($head, 'general_bal', '', -1, "reports@reports");


print ' <link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/css/select2.min.css" rel="stylesheet" /> ';
//print ' <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.0.10/css/all.css" integrity="sha384-+d0P83n9kaQMCwj8F4RJB66tzIwOKmrdb46+porD/OvrJ+37WqIM7UoBtwHO6Nlg" crossorigin="anonymous"> ';
print ' <script src="../js/jquery.treetable.js"></script> ';
print ' <link href="../css/jquery.treetable.css" rel="stylesheet" type="text/css" /> ';
print ' <link href="../css/jquery.treetable.theme.default.css" rel="stylesheet" type="text/css" /> ';


//Temas para Bootstrap
print '
		<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
		<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap-theme.min.css" integrity="sha384-rHyoN1iRsVXV4nD0JutlnGaslCJuC7uwjduW9SVrLvRYooPp2bWYgmgJQIXwl/Sp" crossorigin="anonymous">
		<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>
';


print '<form method="POST" action="'.$_SERVER["PHP_SELF"].'">';
print '<input type="hidden" name="token" value="'.$_SESSION['newtoken'].'">';


$startdate = GETPOST('startdate');
$enddate = GETPOST('enddate');

//$form->select_date($startdate,'ap',0,0,0,"action",1,0,0,0,'fulldaystart');
print '
		<div class="content">
	    <div class="row">
            <div class="col-md-2 form-group">
              <label>Fecha inicio</label>
              <input type="date" id="startdate" name="startdate" class="hasDatepicker" >
            </div>
            <div class="col-md-2 form-group">
              <label>Fecha fin</label>
              <input type="date" id="enddate" name="enddate" >
            </div>
            <div class="col-md-2 form-group">
              <br>
              <button type="submit" class="button">Buscar</button>
            </div>
		</div>';

print '</form>';


if (isset($startdate) && isset($enddate) && $startdate != "" && $enddate != "") 
{
	
	print '
    	<div class="row">
			<div class="col-md-6 form-group">
            	<button id="btnExport" class="btn btn-default">Excel</button>
            	<button id="btnExportPDF" class="btn btn-default">PDF</button>
    		</div>
    		<div class="col-md-6 form-group">
            	<b>Total Activo:</b> <label id="activo_total">$ - </label><br/>
            	<b>Total Pasivo + Capital Contable:</b> <label id="pasivo_capital">$ - </label><br/>
            	<b>Balance:</b> <label id="balance">$ - </label>
            </div>
		</div>
    ';
    
	print '
		<link rel="stylesheet" type="text/css" href="../css/easyui/themes/bootstrap/easyui.css">
   		<link rel="stylesheet" type="text/css" href="../css/easyui/themes/icon.css">
    	
    	<script type="text/javascript" src="../css/easyui/jquery.easyui.min.js"></script>
    	<script>
			$( window ).resize(function() {
			      $("#dg").datagrid("resize");
			});
		</script>	    	
    	';
    
    print '
    	<div class="row">
			<div class="col-md-6 form-group">
            	<table id="dgActivos" title="Activo" class="easyui-treegrid" style="width:100%;height:800px"
		        		data-options="
		            		url: \'providers/get-general_bal.php?option=1&startdate='.$startdate.'&enddate='.$enddate.'\',
		            		rownumbers: false,
		            		//pagination: true,
		            		pageSize: 15,
		            		pageList: [15,40,80],
		            		idField: \'id\',
		            		treeField: \'text\',
		            		onBeforeLoad: function(row,param){
		                		if (!row) {    // load top level rows
		                    		param.id = 0;    // set id=0, indicate to load new page rows
		                		}
		            		},
		            		loadFilter: function(rows){
						        return convert(rows);
						    }
		        		">
		    	    <thead>
		    	        <tr>
		                    <th field="cta" >Cuenta</th>
		    	            <th field="text" width="350" >Concepto</th>
		    	            <th field="saldo" width="100" align="right" formatter="formatMoney" styler="formatcolor">Movimientos</th>
		    	        </tr>
		    	    </thead>
		    	</table>
    		</div>
    		<div class="col-md-6 form-group">
    			<div class="row">
    				<div class="col-md-12 form-group">
	            	<table id="dgPasivo" title="Pasivo" class="easyui-treegrid" style="width:100%;height:400px"
			        		data-options="
			            		url: \'providers/get-general_bal.php?option=2&startdate='.$startdate.'&enddate='.$enddate.'\',
		            			rownumbers: true,
			            		//pagination: true,
			            		pageSize: 15,
			            		pageList: [15,40,80],
			            		idField: \'id\',
			            		treeField: \'text\',
			            		onBeforeLoad: function(row,param){
			                		if (!row) {    // load top level rows
			                    		param.rowid = 0;    // set id=0, indicate to load new page rows
			                		}
			            		},
			            		loadFilter: function(rows){
							        return convert(rows);
							    }
			        		">
			    	    <thead>
			    	        <tr>
			                    <th field="cta" >Cuenta</th>
			    	            <th field="text" width="350" >Concepto</th>
			    	            <th field="saldo" width="100" align="right" formatter="formatMoney" styler="formatcolor">Movimientos</th>
			    	        </tr>
			    	    </thead>
			    	</table>
			    	</div>
			    </div>
			    <div class="row">
			    	<div class="col-md-12 form-group">
		            	<table id="dgCapital" title="Capital" class="easyui-treegrid" style="width:100%;height:400px"
				        		data-options="
				            		url: \'providers/get-general_bal.php?option=3&startdate='.$startdate.'&enddate='.$enddate.'\',
		            				rownumbers: true,
				            		//pagination: true,
				            		pageSize: 15,
				            		pageList: [15,40,80],
				            		idField: \'id\',
				            		treeField: \'text\',
				            		onBeforeLoad: function(row,param){
				                		if (!row) {    // load top level rows
				                    		param.rowid = 0;    // set id=0, indicate to load new page rows
				                		}
				            		},
				            		loadFilter: function(rows){
								        return convert(rows);
								    }
				        		">
				    	    <thead>
				    	        <tr>
				                    <th field="cta" >Cuenta</th>
				    	            <th field="text" width="350" >Concepto</th>
				    	            <th field="saldo" width="100" align="right" formatter="formatMoney" styler="formatcolor">Movimientos</th>
				    	        </tr>
				    	    </thead>
				    	</table>
			    	</div>
			    </div>
    		</div>
		</div>
    ';
	

}

print '
		<script>
    	    function formatDollar(value){
    	        if (value){
    	        	if(parseFloat(value) >= 0)
    	            	return "$" + parseFloat(value).toFixed(2);
    	           	else
    	           		return "$(" + Math.abs(parseFloat(value)).toFixed(2) + ")";
    	        } else {
    	            return "";
    	        }
    	    }
            
            function formatcolor(value,row){
                if (value < 0){
                    return \'color:red\';
                }
            }
            
            function formatMoney(amount, decimalCount = 2, decimal = ".", thousands = ",") {
			  try {
			    decimalCount = Math.abs(decimalCount);
			    decimalCount = isNaN(decimalCount) ? 2 : decimalCount;
			
			    const negativeSign = amount < 0 ? "$ (" : "$ ";
				const negativeSign_end = amount < 0 ? ")" : "";
				
			    let i = parseInt(amount = Math.abs(Number(amount) || 0).toFixed(decimalCount)).toString();
			    let j = (i.length > 3) ? i.length % 3 : 0;
			
			    return negativeSign + (j ? i.substr(0, j) + thousands : \'\') + i.substr(j).replace(/(\d{3})(?=\d)/g, "$1" + thousands) + (decimalCount ? decimal + Math.abs(amount - i).toFixed(decimalCount).slice(2) : "") + negativeSign_end;
			  } catch (e) {
			    console.log(e)
			  }
			}
			
			function convert(rows){
			    function exists(rows, parentId){
			        for(var i=0; i<rows.length; i++){
			            if (rows[i].id == parentId) return true;
			        }
			        return false;
			    } 
			    
			    var nodes = [];
			    // get the top level nodes
			    if(rows.constructor == Array)
			    {
				    for(var i=0; i<rows.length; i++){
				        var row = rows[i];
				        if (!exists(rows, row.parentId)){
				            nodes.push({
				            	id:row.id,
				                text:row.name,
				                cta: row.cta,
				                saldo: row.saldo
				                /*
				                *  AQUI AGREGAR NODOS
				                */
				            });
				        }
				    }
			    }else
			    {
			    	var row = rows;
			        if (!exists(rows, row.parentId)){
			            nodes.push({
			            	id:row.id,
			                text:row.name,
			                cta: row.cta,
			                saldo: row.saldo
			                /*
			                *  AQUI AGREGAR NODOS
			                */
			            });
			        }
			    }
			    
			    var toDo = [];
			    for(var i=0; i<nodes.length; i++){
			        toDo.push(nodes[i]);
			    }
			    while(toDo.length){
			        var node = toDo.shift();    // the parent node
			        // get the children nodes
			        for(var i=0; i<rows.length; i++){
			            var row = rows[i];
			            if (row.parentId == node.id){
			                var child = 
			                	{
			                		state: row.state == null ? "closed" : row.state,
			                		id:row.id,
			                		text:row.name,
			                		cta: row.cta, 
			                		saldo: row.saldo
			                	}; 
			                	
			                if (node.children){
			                    node.children.push(child);
			                } else {
			                    node.children = [child];
			                }
			                toDo.push(child);
			            }
			        }
			    } 
			    return nodes;
			}
            
    	</script>
    	
		<script>

            $(document).ready(function()
            {
            	document.getElementById("startdate").value = "'.$startdate.'";
            	document.getElementById("enddate").value = "'.$enddate.'";
                
                $("#btnExport").click(function(e) {
                	e.preventDefault(); 
					window.open(\'../lib/phpspreadsheet/excel-export-general-bal.php?startdate='.$startdate.'&enddate='.$enddate.' \');
					e.preventDefault(); 
				});
				
				$("#btnExportPDF").click(function(e) {
                	e.preventDefault(); 
					window.open(\'../lib/pdf/general_bal_pdf.php?startdate='.$startdate.'&enddate='.$enddate.' \');
					e.preventDefault(); 
				});
				
				$.ajax({
		            url : \'providers/general-bal/get-active-val.php?startdate='.$startdate.'&enddate='.$enddate.'\',
		            success : function (data) {
		            	if(data < 0)
		                	$("#activo_total").html(formatMoney(data)).css("color","red");
		                else
		                	$("#activo_total").html(formatMoney(data));
		                	
		                var activo = data;
		                
		                $.ajax({
				            url : \'providers/general-bal/get-pasivo-capital-val.php?startdate='.$startdate.'&enddate='.$enddate.'\',
				            success : function (data) {
				                if(data < 0)
				                	$("#pasivo_capital").html(formatMoney(data)).css("color","red");
				                else
				                	$("#pasivo_capital").html(formatMoney(data));
				                	
				                var balance = (Math.round(activo * 100) / 100) - (Math.round(data * 100) / 100); 
				                if(balance < 0)
				                	$("#balance").html(formatMoney(activo - data)).css("color","red");
				                else
				                	$("#balance").html(formatMoney(activo - data))
				            }
				        });
		                
		            }
		        });
                
            });
            
			
		</script>
	';
	



llxFooter();

$db->close();