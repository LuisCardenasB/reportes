<?php
/* Copyright (C) 2004-2017 Laurent Destailleur  <eldy@users.sourceforge.net>
 * Copyright (C) ---Put here your own copyright and developer email---
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/**
 * \file    htdocs/modulebuilder/template/admin/about.php
 * \ingroup mymodule
 * \brief   About page of module MyModule.
 */

// Load Dolibarr environment
$res=0;
// Try main.inc.php into web root known defined into CONTEXT_DOCUMENT_ROOT (not always defined)
if (! $res && ! empty($_SERVER["CONTEXT_DOCUMENT_ROOT"])) $res=@include($_SERVER["CONTEXT_DOCUMENT_ROOT"]."/main.inc.php");
// Try main.inc.php into web root detected using web root caluclated from SCRIPT_FILENAME
$tmp=empty($_SERVER['SCRIPT_FILENAME'])?'':$_SERVER['SCRIPT_FILENAME'];$tmp2=realpath(__FILE__); $i=strlen($tmp)-1; $j=strlen($tmp2)-1;
while($i > 0 && $j > 0 && isset($tmp[$i]) && isset($tmp2[$j]) && $tmp[$i]==$tmp2[$j]) { $i--; $j--; }
if (! $res && $i > 0 && file_exists(substr($tmp, 0, ($i+1))."/main.inc.php")) $res=@include(substr($tmp, 0, ($i+1))."/main.inc.php");
if (! $res && $i > 0 && file_exists(dirname(substr($tmp, 0, ($i+1)))."/main.inc.php")) $res=@include(dirname(substr($tmp, 0, ($i+1)))."/main.inc.php");
// Try main.inc.php using relative path
if (! $res && file_exists("../../main.inc.php")) $res=@include("../../main.inc.php");
if (! $res && file_exists("../../../main.inc.php")) $res=@include("../../../main.inc.php");
if (! $res) die("Include of main fails");


require_once DOL_DOCUMENT_ROOT.'/core/class/html.formfile.class.php';
require_once '../lib/reports.lib.php';


$langs->loadLangs(array("reports@reports"));

$action=GETPOST('action', 'alpha');


// Securite acces client
if (! $user->rights->reports->read) accessforbidden();
$socid=GETPOST('socid','int');
if (isset($user->societe_id) && $user->societe_id > 0)
{
	$action = '';
	$socid = $user->societe_id;
}

$max=5;
$now=dol_now();


/*
 * Actions
 */

// None


/*
 * View
 */

$form = new Form($db);
$formfile = new FormFile($db);

llxHeader("",$langs->trans("Reportes contables"));

print load_fiche_titre($langs->trans("Reportes contables"),'','object_reports.png@reports');

//print '<div class="fichecenter"><div class="fichethirdleft">';

print "Seleccione el reporte que desea consultar.";

$head = reportsUserPrepareHead();
dol_fiche_head($head, 'auxiliar', '', -1, "reports@reports");


print ' <link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/css/select2.min.css" rel="stylesheet" /> ';
//print ' <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.0.10/css/all.css" integrity="sha384-+d0P83n9kaQMCwj8F4RJB66tzIwOKmrdb46+porD/OvrJ+37WqIM7UoBtwHO6Nlg" crossorigin="anonymous"> ';
print ' <script src="../js/jquery.treetable.js"></script> ';
print ' <link href="../css/jquery.treetable.css" rel="stylesheet" type="text/css" /> ';
print ' <link href="../css/jquery.treetable.theme.default.css" rel="stylesheet" type="text/css" /> ';


//Temas para Bootstrap
print '
		<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
		<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap-theme.min.css" integrity="sha384-rHyoN1iRsVXV4nD0JutlnGaslCJuC7uwjduW9SVrLvRYooPp2bWYgmgJQIXwl/Sp" crossorigin="anonymous">
		<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>
';


print '<form method="POST" action="'.$_SERVER["PHP_SELF"].'">';
print '<input type="hidden" name="token" value="'.$_SESSION['newtoken'].'">';

$cta = GETPOST('cta');
$cta2 = GETPOST('cta2');
$startdate = GETPOST('startdate');
$enddate = GETPOST('enddate');
$cta_desc = "";
$cta2_desc = "";


if(isset($cta))
{
	$sql = "select rowid as id, concat(cta, ' - ', descta) as text from ".MAIN_DB_PREFIX."contab_cat_ctas where rowid = '" . $cta . "' ";
	$resql = $db->query($sql);
	while($row = $db->fetch_object($resql)) 
	{
		$cta_desc = 
			'
				var newOption = new Option("'.$row->text.'", '.$cta.', true, true);
				$("#square").append(newOption).trigger("change");
			';
	}
}

if(isset($cta2))
{
	$sql = "select rowid as id, concat(cta, ' - ', descta) as text from ".MAIN_DB_PREFIX."contab_cat_ctas where rowid = '" . $cta2 . "' ";
	$resql = $db->query($sql);
	while($row = $db->fetch_object($resql)) 
	{
		$cta2_desc = 
			'
				var newOption = new Option("'.$row->text.'", '.$cta2.', true, true);
				$("#square2").append(newOption).trigger("change");
			';
	}
}

print '
		<div class="content">
	    <div class="row">
			<div class="col-md-3 form-group">
              <label>Cuenta Inicio</label>
              <select id="square" name="cta" class="form-control">
              </select>
            </div>
            <div class="col-md-3 form-group">
              <label>Cuenta Fin</label>
              <select id="square2" name="cta2" class="form-control">
              </select>
            </div>
            <div class="col-md-2 form-group">
              <label>Fecha inicio</label>
              <input type="date" id="startdate" name="startdate" >
            </div>
            <div class="col-md-2 form-group">
              <label>Fecha fin</label>
              <input type="date" id="enddate" name="enddate" >
            </div>
            <div class="col-md-2 form-group">
              <br>
              <button type="submit" class="button">Buscar</button>
            </div>
		</div>';

print '</form>';


if (isset($cta) && isset($startdate) && isset($enddate) && $cta != "" && $startdate != "" && $enddate != "") 
{
	
	print '
    	<div class="row">
			<div class="col-md-4 form-group">
            	<button id="btnExport" class="btn btn-default">Excel</button>
            	<button id="btnExportPDF" class="btn btn-default">PDF</button>
    		</div>
		</div>
    ';
    
    print '
		<link rel="stylesheet" type="text/css" href="../css/easyui/themes/bootstrap/easyui.css">
   		<link rel="stylesheet" type="text/css" href="../css/easyui/themes/icon.css">
    	
    	<script type="text/javascript" src="../css/easyui/jquery.easyui.min.js"></script>
    	<script>
			$( window ).resize(function() {
			      $("#dg").datagrid("resize");
			});
		</script>	    	
    	';

	print '
    	<table id="dg" title="Cuentas" class="easyui-treegrid" style="width:100%;height:500px"
        		data-options="
            		url: \'providers/get-counts.php?cta='.$cta.'&startdate='.$startdate.'&enddate='.$enddate.'&cta2='.$cta2.'\',
            		rownumbers: true,
            		pagination: true,
            		pageSize: 15,
            		pageList: [15,40,80],
            		idField: \'rowid\',
            		treeField: \'descta\',
            		onBeforeLoad: function(row,param){
                		if (!row) {    // load top level rows
                    		param.rowid = 0;    // set id=0, indicate to load new page rows
                		}
            		},
                    onDblClickCell: function(field,row)
                    {
                    	if(row[\'tipo_pol\'] != null)
                        {
                        	if(field == \'descta\')
                            {
                            	var d = new Date(row[\'fecha\'] + \' 00:00:00.000\');
								var month = d.getMonth() + 1;
                                var year = d.getFullYear();
                            	var win = window.open(\'../../contab/lists/libro_diario.php?a=\'+ year +\'&m=\'+ month +\'&id=\'+row[\'rowid\'], \'_blank\');
  								win.focus();
                            }
                        }
                    	
                    }
        		">
    	    <thead>
    	        <tr>
                	<th field="fecha" >Fecha</th>
                    <th field="cta" >Cuenta</th>
                    <th field="tipo_pol" >Tipo</th>
                    <th field="cons" >Consecutivo</th>
                    
    	            <th field="descta" width="400" >Concepto</th>
    	            <th field="saldo_inicial" width="100" align="right" formatter="formatMoney">Saldo inicial</th>
    	            <th field="debe" width="100" align="right" formatter="formatMoney">Debe</th>
    	            <th field="haber" width="100" align="right" formatter="formatMoney">Haber</th>
    	            <th field="saldo" width="100" align="right" formatter="formatMoney" styler="formatcolor">Saldo</th>
    	        </tr>
    	    </thead>
    	</table>
    	<script>
    	    function formatDollar(value){
    	        if (value){
    	            return "$" + parseFloat(value).toFixed(2);
    	        } else {
    	            return "";
    	        }
    	    }
            
            function formatcolor(value,row){
                if (value < 0){
                    return \'color:red\';
                }
            }
            
            function formatMoney(amount, decimalCount = 2, decimal = ".", thousands = ",") {
			  try {
			    decimalCount = Math.abs(decimalCount);
			    decimalCount = isNaN(decimalCount) ? 2 : decimalCount;
			
			    const negativeSign = amount < 0 ? "$ (" : "$ ";
				const negativeSign_end = amount < 0 ? ")" : "";
				
			    let i = parseInt(amount = Math.abs(Number(amount) || 0).toFixed(decimalCount)).toString();
			    let j = (i.length > 3) ? i.length % 3 : 0;
			
			    return negativeSign + (j ? i.substr(0, j) + thousands : \'\') + i.substr(j).replace(/(\d{3})(?=\d)/g, "$1" + thousands) + (decimalCount ? decimal + Math.abs(amount - i).toFixed(decimalCount).slice(2) : "") + negativeSign_end;
			  } catch (e) {
			    console.log(e)
			  }
			}
    	</script>
	';

}


print '
		<script>
        	/*
			$("#report").treetable(
            { 
            	expandable: true,
                onNodeExpand: function()
                {
                	console.log(this);
                }
            });
			
			$("#report tbody").on("mousedown", "tr", function() {
			  $(".selected").not(this).removeClass("selected");
			  $(this).toggleClass("selected");
			});
			*/
            
            
            var options = {
            	placeholder: "Seleccione cuenta inicio",
      			ajax: {
        			url: "providers/cuentas.php",
        			dataType: "json",
        			delay: 250,
        			data: function (params) {
          				return {
            				q: params.term, // search term
            				page: params.page,
            				option: 1
          				};
        			},
        			processResults: function (data, params) {
          				return {
            				results: data,
          				};
        			},
        			cache: true
      			},
      			escapeMarkup: function (markup) { return markup; }, // let our custom formatter work
      				minimumInputLength: 0,
    		};
    		
    		var options2 = {
            	placeholder: "Seleccione cuenta fin",
      			ajax: {
        			url: "providers/cuentas.php",
        			dataType: "json",
        			delay: 0,
        			data: function (params) 
        			{
          				return {
            				q: params.term, // search term
            				page: params.page,
            				option: 2,
            				start_select: document.getElementById("square").value
          				};
        			},
        			processResults: function (data, params) {
        				$("#square2").val(null).trigger("change");
          				return {
            				results: data,
          				};
        			},
        			cache: false,
        			allowClear: true
      			},
      			escapeMarkup: function (markup) { return markup; }, // let our custom formatter work
      				minimumInputLength: 0,
    		};
            
			
            
            $(document).ready(function()
            {
            	$("#square").select2(options);
            	$("#square2").select2(options2);
            	
            	$("#square").on("select2:select", function (e) {
				    var data = e.params.data;
				    $("#square2").val(null).trigger("change");
				});
				
                '. $cta_desc. '
                '. $cta2_desc. '
				
            	document.getElementById("startdate").value = "'.$startdate.'";
            	document.getElementById("enddate").value = "'.$enddate.'";
                
                $("#btnExport").click(function(e) {
                	e.preventDefault(); 
					window.open(\'../lib/phpspreadsheet/excel-export-auxiliar.php?startdate='.$startdate.'&enddate='.$enddate.'&cta='.$cta.'&type=auxiliar&cta2='.$cta2.'\');
					e.preventDefault(); 
				});
				
				$("#btnExportPDF").click(function(e) {
                	e.preventDefault(); 
					window.open(\'../lib/pdf/auxiliar_pdf.php?startdate='.$startdate.'&enddate='.$enddate.'&cta='.$cta.'&type=auxiliar&cta2='.$cta2.'\');
					e.preventDefault(); 
				});
                
                $(\'#expandall\').click(function(e){
                	e.preventDefault();
                    
    				var dg = $(\'#dg\');
                    dg.treegrid(\'expandAll\');
                    
    				e.preventDefault();
   	 				return false;
				});
            });
            
			
		</script>
	';
	



llxFooter();

$db->close();