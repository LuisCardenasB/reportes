<?php
/* Copyright (C) 2004-2017 Laurent Destailleur  <eldy@users.sourceforge.net>
 * Copyright (C) ---Put here your own copyright and developer email---
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/**
 * \file    htdocs/modulebuilder/template/admin/about.php
 * \ingroup mymodule
 * \brief   About page of module MyModule.
 */
 
// Load Dolibarr environment
$res=0;
// Try main.inc.php into web root known defined into CONTEXT_DOCUMENT_ROOT (not always defined)
if (! $res && ! empty($_SERVER["CONTEXT_DOCUMENT_ROOT"])) $res=@include($_SERVER["CONTEXT_DOCUMENT_ROOT"]."/main.inc.php");
// Try main.inc.php into web root detected using web root caluclated from SCRIPT_FILENAME
$tmp=empty($_SERVER['SCRIPT_FILENAME'])?'':$_SERVER['SCRIPT_FILENAME'];$tmp2=realpath(__FILE__); $i=strlen($tmp)-1; $j=strlen($tmp2)-1;
while($i > 0 && $j > 0 && isset($tmp[$i]) && isset($tmp2[$j]) && $tmp[$i]==$tmp2[$j]) { $i--; $j--; }
if (! $res && $i > 0 && file_exists(substr($tmp, 0, ($i+1))."/main.inc.php")) $res=@include(substr($tmp, 0, ($i+1))."/main.inc.php");
if (! $res && $i > 0 && file_exists(dirname(substr($tmp, 0, ($i+1)))."/main.inc.php")) $res=@include(dirname(substr($tmp, 0, ($i+1)))."/main.inc.php");
// Try main.inc.php using relative path
if (! $res && file_exists("../../main.inc.php")) $res=@include("../../main.inc.php");
if (! $res && file_exists("../../../main.inc.php")) $res=@include("../../../main.inc.php");
if (! $res) die("Include of main fails");

require_once DOL_DOCUMENT_ROOT.'/core/class/html.formfile.class.php';
require_once '../lib/reports.lib.php';

$langs->loadLangs(array("reports@reports"));
$action=GETPOST('action', 'alpha');


// Securite acces client
if (! $user->rights->reports->read) accessforbidden();
$socid=GETPOST('socid','int');
if (isset($user->societe_id) && $user->societe_id > 0)
{
	$action = '';
	$socid = $user->societe_id;
}

$max=5;
$now=dol_now();

$form = new Form($db);
$formfile = new FormFile($db);

llxHeader("",$langs->trans("Pólizas"));

print load_fiche_titre($langs->trans("Pólizas"),'','object_reports.png@reports');

print "Seleccione el reporte que desea consultar.";

$head = reportsUserPrepareHead();
dol_fiche_head($head, 'polizas', '', -1, "reports@reports");


print '
		<link rel="stylesheet" type="text/css" href="lib/easyui/themes/bootstrap/easyui.css">
   		<link rel="stylesheet" type="text/css" href="lib/easyui/themes/icon.css">
    	
    	<script type="text/javascript" src="lib/easyui/jquery.easyui.min.js"></script>
    	<script>
			$( window ).resize(function() {
			      $("#dg").datagrid("resize");
			});
		</script>	    	
    	';
    	
    	
    	
	print '<div class="fichecenter">';
	print '<div class="underbanner clearboth"></div>';

	print '<table class="border" width="100%">';

	// Resource type
	/*
	print '<tr>';
	print '<td class="titlefield">' . $langs->trans("ResourceType") . '</td>';
	print '<td>';
	print $object->type_label;
	print '</td>';
	print '</tr>';
	*/
	print "</table>";

	print '</div>';
	print '<br>';
	
	$permission=$user->rights->resource->write;
	$cssclass='titlefield';
?>
	
	 <table id="dg" class="easyui-datagrid" title="INFORME DE POLIZAS" style="height:500px"
            data-options="singleSelect:true,collapsible:true,showFooter:true,url:'providers/get-poliza.php',method:'post',pagination:'true',toolbar:'#tb'">
        <thead>
            <tr>
            	<th data-options="field:'itemid',width:30,align:'center'">ID</th>
                <th data-options="field:'po',width:90,align:'center', formatter:formatPO">POLIZA</th>
                <th data-options="field:'document',width:180,align:'center', formatter:formatInvoice">DOCUMENTO RELACIONADO</th>
                <th data-options="field:'prov',width:100,align:'center'">TERCERO</th>
                <th data-options="field:'fecha',width:180,align:'center'">FECHA FACTURACION</th>
                <th data-options="field:'famount',width:100,align:'center'">IMPORTE</th>
                <th data-options="field:'ftotal_tva',width:100,align:'left'">IVA</th>
                <th data-options="field:'ftotal_ttc',width:100,align:'left'">TOTAL</th>
            </tr>
        </thead>
    </table>
    

    <div id="tb" style="padding:2px 5px;">
        Desde: <input class="easyui-datebox" style="width:110px">
        Hasta: <input class="easyui-datebox" style="width:110px">
        <a href="#" class="easyui-linkbutton" iconCls="icon-search">Buscar</a>
        <a href="#" id="btnExportPDF" class="easyui-linkbutton" iconCls="icon-print">Exportar a PDF</a>
    </div>

   <script type="text/javascript" src="lib/easyui/datagrid-filter.js"></script>
   <script type="text/javascript">

        
        $(function(){
            var dg = $('#dg').datagrid({
                url: 'providers/get-poliza.php',
                pagination: true,
                clientPaging: false,
                remoteFilter: true,
            });
            dg.datagrid('enableFilter', [{
                field:'status',
                type:'text',
                options:{precision:1},
                op:['contains','beginwith']
            },{
                field:'total_tva',
                type:'numberbox',
                options:{precision:1},
                op:['less','greater']
            }]);
            
            var data = $('#dg').datagrid('getData');
			var rows = data.firstRows;
			console.log(rows);
        });
        
        
        function formatPO(val,row){
        	if(row.po_id != null){
			    var url = "<?php echo DOL_URL_ROOT ?>/fourn/commande/card.php?id=";
			    return '<a href="'+url + row.po_id+'">'+val+'</a>';
        	}
		}
		
        function formatInvoice(val,row){
        	if(row.fk_target != null){
			    var url = "<?php echo DOL_URL_ROOT ?>/fourn/facture/card.php?facid=";
			    return '<a href="'+url + row.fk_target+'">'+val+'</a>';
        	}
		}
		
    </script>
    
    <!--<script type="text/javascript" src="lib/jquery/jquery-redirect.js"></script>-->
  <!--  <script>-->
  <!--  	$(document).ready(function(){-->
		<!--		$("#btnExportPDF").click(function(e) {-->
					
		<!--			var items = $('#dg').datagrid('getRows');-->
					
					
  <!--              	e.preventDefault(); -->
		<!--			$.redirect('providers/report-service.php', {items, resource: <?php echo $id?>}, "POST", "_blank"); -->
		<!--			e.preventDefault(); -->
		<!--		});-->
		<!--});	-->
  <!--  </script>-->
    
<?php
	dol_fiche_end();
// }

// End of page
llxFooter();
$db->close();