<?php
	header('Content-type:application/json;charset=utf-8');
	
	$res=0;
	// Try main.inc.php into web root known defined into CONTEXT_DOCUMENT_ROOT (not always defined)
	if (! $res && ! empty($_SERVER["CONTEXT_DOCUMENT_ROOT"])) $res=@include($_SERVER["CONTEXT_DOCUMENT_ROOT"]."/main.inc.php");
	// Try main.inc.php into web root detected using web root caluclated from SCRIPT_FILENAME
	$tmp=empty($_SERVER['SCRIPT_FILENAME'])?'':$_SERVER['SCRIPT_FILENAME'];$tmp2=realpath(__FILE__); $i=strlen($tmp)-1; $j=strlen($tmp2)-1;
	while($i > 0 && $j > 0 && isset($tmp[$i]) && isset($tmp2[$j]) && $tmp[$i]==$tmp2[$j]) { $i--; $j--; }
	if (! $res && $i > 0 && file_exists(substr($tmp, 0, ($i+1))."/main.inc.php")) $res=@include(substr($tmp, 0, ($i+1))."/main.inc.php");
	if (! $res && $i > 0 && file_exists(dirname(substr($tmp, 0, ($i+1)))."/main.inc.php")) $res=@include(dirname(substr($tmp, 0, ($i+1)))."/main.inc.php");
	// Try main.inc.php using relative path
	if (! $res && file_exists("../../main.inc.php")) $res=@include("../../main.inc.php");
	if (! $res && file_exists("../../../main.inc.php")) $res=@include("../../../main.inc.php");
	if (! $res) die("Include of main fails");

	if (file_exists(DOL_DOCUMENT_ROOT . '/reports/class/accounts.class.php')) {
		require_once DOL_DOCUMENT_ROOT . '/reports/class/accounts.class.php';
	}
	
	global $langs, $user, $conf, $db;

	$ctas_functions = new Accounts($db);
	
	$page = isset($_POST['page']) ? intval($_POST['page']) : 1;
	$rows = isset($_POST['rows']) ? intval($_POST['rows']) : 10;
	$offset = ($page-1)*$rows;

	$id = GETPOST('id');
	$id = isset($id) && $id != "" ? $id : "NO DATA" ;
	
	$startdate = GETPOST('startdate');
    $enddate = GETPOST('enddate');
    
    $json = array();
	$data = array();

	if($id == "NO DATA")
	{
		/*****
		 * 
		 * 
		 * 
		*/
		//Ventas
		$ventas = 0;
		$cta_venta = $ctas_functions->fetch_ventas_acount();
			
			
		$sql = "SELECT cc.rowid, cc.cta, cc.descta, 0 as saldo, 'closed' as state FROM ".MAIN_DB_PREFIX."contab_cat_ctas as cc
				inner join ".MAIN_DB_PREFIX."contab_sat_ctas sc on cc.fk_sat_cta = sc.rowid
				where cc.subctade = $cta_venta";
	
		$resql = $db->query($sql);				

	
		$resql = $db->query($sql);
		$data_tmp = array();
		while($row = $db->fetch_object($resql)) 
		{
			$value = $ctas_functions->fetch_mounts_of_period($row->rowid, $startdate, $enddate);
			if($value["status"] == 1)
			{
				$row->saldo = strval($value["saldo"]);
				$ventas = $ventas + floatval($value["saldo"]);
				array_push($data_tmp, $row);
			}
			
		}
		array_push($data, array("rowid" => "ID1", "cta" => "", "descta" => "INGRESOS POR VENTAS", "saldo" => strval($ventas), "children" => $data_tmp));
		
		/*****
		 * 
		 * 
		 * 
		*/
		//Costo de operaciòn
		$costos = 0;
		$cta_costos = $ctas_functions->fetch_costos_acounts();
		$sql = "SELECT cc.rowid, cc.cta, cc.descta, 0 as saldo, 'closed' as state FROM ".MAIN_DB_PREFIX."contab_cat_ctas as cc
				inner join ".MAIN_DB_PREFIX."contab_sat_ctas sc on cc.fk_sat_cta = sc.rowid
				where cc.rowid in (" . implode (", ", $cta_costos) . ")";
				
		
		$resql = $db->query($sql);
		$data_tmp = array();		

		
		while($row = $db->fetch_object($resql)) 
		{
			$value = $ctas_functions->fetch_mounts_of_period($row->rowid, $startdate, $enddate);
			if($value["status"] == 1)
			{
				$row->saldo = strval($value["saldo"]);
				$costos = $costos + floatval($value["saldo"]);
				array_push($data_tmp, $row);
			}
			
		}
		array_push($data, array("rowid" => "ID2", "cta" => "", "descta" => "COSTOS DE VENTA", "saldo" => strval($costos), "children" => $data_tmp));
		
		$ut_bruta_calc = $ventas - $costos;
			$text_ut_bruta_calc = "UTILIDAD BRUTA";
		if($ut_bruta_calc < 0)
			$text_ut_bruta_calc = "PERDIDA BRUTA";
			
		array_push($data, array("rowid" => "ID10", "cta" => "", "descta" => $text_ut_bruta_calc, "saldo" => strval($ut_bruta_calc), "children" => null, "iconCls" => "icon-sum" ));
		
		/*****
		 * 
		 * 
		 * 
		*/
		//Gastos
		$gastos = 0;
		$cta_gastos = $ctas_functions->fetch_gastos_acounts();
		$sql = "SELECT cc.rowid, cc.cta, cc.descta, 0 as saldo, 'closed' as state FROM ".MAIN_DB_PREFIX."contab_cat_ctas as cc
				inner join ".MAIN_DB_PREFIX."contab_sat_ctas sc on cc.fk_sat_cta = sc.rowid
				where cc.rowid in (" . implode (", ", $cta_gastos) . ")";
				
		$resql = $db->query($sql);
		$data_tmp = array();
		
		while($row = $db->fetch_object($resql)) 
		{
			$value = $ctas_functions->fetch_mounts_of_period($row->rowid, $startdate, $enddate);
			if($value["status"] == 1)
			{
				$row->saldo = strval($value["saldo"]);
				$gastos = $gastos + floatval($value["saldo"]);
				array_push($data_tmp, $row);
			}
			
		}
		array_push($data, array("rowid" => "ID3", "cta" => "", "descta" => "GASTOS DE OPERACIÓN", "saldo" => strval($gastos), "children" => $data_tmp));
		
		$costos = $costos + $gastos;
		
		
		$ut_operacion_calc = $ventas - $costos;
			$text_ut_operacion_calc = "UTILIDAD DE OPERACIÓN";
		if($ut_operacion_calc < 0)
			$text_ut_operacion_calc = "PERDIDA DE OPERACIÓN";
			
		array_push($data, array("rowid" => "ID11", "cta" => "", "descta" => $text_ut_operacion_calc, "saldo" => strval($ut_operacion_calc), "children" => null, "iconCls" => "icon-sum" ));
		
		
		/*****
		 * 
		 * 
		 * 
		*/
		//Gastos Financieros
		$gastos = 0;
		$cta_gastos = $ctas_functions->fetch_gastos_acount();
		$sql = "SELECT cc.rowid, cc.cta, cc.descta, 0 as saldo, 'closed' as state FROM ".MAIN_DB_PREFIX."contab_cat_ctas as cc
				inner join ".MAIN_DB_PREFIX."contab_sat_ctas sc on cc.fk_sat_cta = sc.rowid
				where cc.subctade = $cta_gastos";
		
		$resql = $db->query($sql);
		$data_tmp = array();
		
		while($row = $db->fetch_object($resql)) 
		{
			$value = $ctas_functions->fetch_mounts_of_period($row->rowid, $startdate, $enddate);
			if($value["status"] == 1)
			{
				$row->saldo = strval($value["saldo"]);
				$gastos = $gastos + floatval($value["saldo"]);
				array_push($data_tmp, $row);
			}
			
		}
		array_push($data, array("rowid" => "ID4", "cta" => "", "descta" => "RESULTADO INTEGRAL DE FINANCIAMIENTO", "saldo" => strval($gastos), "children" => $data_tmp));
		
		
		
		//SE LE PONE VALOR ABOSULUTO DONDE POSIBLEMENTE NO LLEVA
		if($gastos < 0)
			$gastos = abs($gastos);
			
		$utilidad_antes_impuestos = $ut_operacion_calc - $gastos;
		if($utilidad_antes_impuestos >= 0)
			array_push($data, array("rowid" => "ID12", "cta" => "", "descta" => "UTILIDAD ANTES DE IMPUESTOS A LA UTILIDAD", "saldo" => strval($utilidad_antes_impuestos), "children" => null, "iconCls" => "icon-sum" ));
		else
			array_push($data, array("rowid" => "ID12", "cta" => "", "descta" => "PERDIDA ANTES DE IMPUESTOS A LA UTILIDAD", "saldo" => strval($utilidad_antes_impuestos), "children" => null, "iconCls" => "icon-sum" ));
		
		//Get all applicable Taxes
		$taxes = $ctas_functions->get_applicable_taxes();
		$utilidad_neta = $utilidad_antes_impuestos;
		for($i = 0; $i < count($taxes); $i++ )
		{
			$utilidad_menos_impuesto = $utilidad_antes_impuestos * $taxes[$i]["value"];
			array_push($data, array("rowid" => "IDTAX" . $taxes[$i]["id"], "cta" => "", "descta" => $taxes[$i]["name"], "saldo" => strval($utilidad_menos_impuesto), "children" => null, "iconCls" => "icon-sum" ));
			$utilidad_neta = $utilidad_neta - $utilidad_menos_impuesto;
		}
		
		if($utilidad_neta >= 0)
			array_push($data, array("rowid" => "ID6", "cta" => "", "descta" => "UTILIDAD NETA", "saldo" => strval($utilidad_neta), "children" => null, "iconCls" => "icon-sum" ));
		else
			array_push($data, array("rowid" => "ID6", "cta" => "", "descta" => "PERDIDA NETA", "saldo" => strval($utilidad_neta), "children" => null, "iconCls" => "icon-sum" ));
		
		
		
		$json["rows"] = $data;
	    
		echo json_encode($json);
	}else
	{
		//cc.rowid, cc.cta, cc.descta, 0 as saldo
		$sql = "select ctas.rowid, cta, descta,
                 	sum((case when natur = 'A' then ((debe-haber) * -1) else (debe-haber) end)) as saldo 
                from( select rowid,cta,descta,subctade,natur 
					from ( select c.*, sc.natur
                      from ".MAIN_DB_PREFIX."contab_cat_ctas as c 
                      inner join ".MAIN_DB_PREFIX."contab_sat_ctas as sc on c.fk_sat_cta = sc.rowid 
                	order by subctade, rowid ) products_sorted, 
				(select @pv := $id) initialisation 
				where (find_in_set(subctade, @pv) > 0 or rowid = $id) and @pv := concat(@pv, ',', rowid)) ctas 
				inner join ".MAIN_DB_PREFIX."contab_polizasdet pdet on pdet.cuenta = ctas.cta 
				INNER JOIN ".MAIN_DB_PREFIX."contab_polizas as p on pdet.fk_poliza = p.rowid 
                where p.fecha between '$startdate 00:00:00.000' and '$enddate 23:59:59.999'
                group by ctas.rowid, cta, descta";
    
    	$resql = $db->query($sql);
    	
    	$data = array();
		while($row = $db->fetch_object($resql)) 
		{
			$saldo_inicial = $ctas_functions->fetch_initial_mounts($row->rowid, $startdate);
			if($saldo_inicial["status"] == 1)
			{
				$row->saldo_inicial = $saldo_inicial["saldo"];
				$row->saldo = strval(floatval($row->saldo) + floatval($saldo_inicial["saldo"]));
			}
			array_push($data, $row);
		}
		
    	echo json_encode($data);
		
	}