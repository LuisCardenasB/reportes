<?php
	header('Content-type:application/json;charset=utf-8');
	
	$res=0;
	// Try main.inc.php into web root known defined into CONTEXT_DOCUMENT_ROOT (not always defined)
	if (! $res && ! empty($_SERVER["CONTEXT_DOCUMENT_ROOT"])) $res=@include($_SERVER["CONTEXT_DOCUMENT_ROOT"]."/main.inc.php");
	// Try main.inc.php into web root detected using web root caluclated from SCRIPT_FILENAME
	$tmp=empty($_SERVER['SCRIPT_FILENAME'])?'':$_SERVER['SCRIPT_FILENAME'];$tmp2=realpath(__FILE__); $i=strlen($tmp)-1; $j=strlen($tmp2)-1;
	while($i > 0 && $j > 0 && isset($tmp[$i]) && isset($tmp2[$j]) && $tmp[$i]==$tmp2[$j]) { $i--; $j--; }
	if (! $res && $i > 0 && file_exists(substr($tmp, 0, ($i+1))."/main.inc.php")) $res=@include(substr($tmp, 0, ($i+1))."/main.inc.php");
	if (! $res && $i > 0 && file_exists(dirname(substr($tmp, 0, ($i+1)))."/main.inc.php")) $res=@include(dirname(substr($tmp, 0, ($i+1)))."/main.inc.php");
	// Try main.inc.php using relative path
	if (! $res && file_exists("../../main.inc.php")) $res=@include("../../main.inc.php");
	if (! $res && file_exists("../../../main.inc.php")) $res=@include("../../../main.inc.php");
	if (! $res) die("Include of main fails");

	if (file_exists(DOL_DOCUMENT_ROOT . '/reports/class/accounts.class.php')) {
		require_once DOL_DOCUMENT_ROOT . '/reports/class/accounts.class.php';
	}
	
	global $langs, $user, $conf, $db;

	$ctas_functions = new Accounts($db);
	
	$page = isset($_POST['page']) ? intval($_POST['page']) : 1;
	$rows = isset($_POST['rows']) ? intval($_POST['rows']) : 10;
	$offset = ($page-1)*$rows;

	$id = GETPOST('id');
	$cta = GETPOST('cta');
	$cta2 = GETPOST('cta2');

	$startdate = GETPOST('startdate');
    $enddate = GETPOST('enddate');

	$id = isset($id) && $id != "" ? $id : "NO DATA" ;
	$cta = isset($cta) && $cta != "" ? $cta : 0 ;
	$cta2 = isset($cta2) && $cta2 != "" ? $cta2 : 0 ;
	$subcta = $id > 0 ? $id : $cta;
	
	//$json = "[";
	$json = array();

	if($id != "NO DATA")
    {
    	$sql = "select *, 0 as saldo_inicial, (debe - haber) as saldo from(
        		select rowid, fecha, tipo_pol, cons, descta, sum(debe) as debe, sum(haber) as haber from (
					SELECT p.rowid as rowid, p.fecha, p.tipo_pol, p.cons, p.concepto as descta, det.debe, det.haber FROM ".MAIN_DB_PREFIX."contab_polizasdet as det
					INNER JOIN ".MAIN_DB_PREFIX."contab_polizas as p on det.fk_poliza = p.rowid
					INNER JOIN ".MAIN_DB_PREFIX."contab_cat_ctas as cta on det.cuenta = cta.cta
					where p.fecha between '".$startdate."' and '".$enddate."' and cta = '".$id."'
				order by fecha ) result group by rowid, fecha, tipo_pol, cons, descta ) result";
    	
    	$resql = $db->query($sql);
		while($row = $db->fetch_object($resql)) 
		{
			$saldo_inicial = $ctas_functions->fetch_initial_mounts($row->rowid, $startdate);
			if($saldo_inicial["status"] == 1)
			{
				$row->saldo_inicial = $saldo_inicial["saldo"];
				$row->saldo = strval(floatval($row->saldo) + floatval($saldo_inicial["saldo"]));
			}
			array_push($json, $row);
		}
    
    }else
    {
    	//Get parent
    	$sql = "select subctade from ".MAIN_DB_PREFIX."contab_cat_ctas where rowid = '" . $cta . "' ";
		$resql = $db->query($sql);
		$parent = "";
		while($row = $db->fetch_object($resql)) 
		{
			$parent = " and subctade = " . $row->subctade;
		}
    	//Get Accounts names
    	$sql = "SELECT cta FROM ".MAIN_DB_PREFIX."contab_cat_ctas where rowid = '" . $cta . "'";
    	$resql = $db->query($sql);
		$cta_desc = "";
		while($row = $db->fetch_object($resql)) 
		{
			$cta_desc = $row->cta;
		}
		$sql_accounts = "SELECT rowid FROM ".MAIN_DB_PREFIX."contab_cat_ctas where cta = '" . $cta_desc . "'";
    	
    	if($cta2 > 0)
    	{
    		$sql = "SELECT cta FROM ".MAIN_DB_PREFIX."contab_cat_ctas where rowid = '" . $cta2 . "'";
    		$resql = $db->query($sql);
			$cta2_desc = "";
			while($row = $db->fetch_object($resql)) 
			{
				$cta2_desc = $row->cta;
			}
			$sql_accounts = "SELECT rowid FROM ".MAIN_DB_PREFIX."contab_cat_ctas where cta between '" . $cta_desc . "' and '" . $cta2_desc . "' $parent ";
    	}
    	
    	//Get parents
    	$resql = $db->query($sql_accounts);
    	$accounts = array();
    	while($row = $db->fetch_object($resql)) 
		{
			array_push($accounts, $row->rowid);
		}
    	
    	$json["total"] = 0;
    	$data = array();
    	for($i = 0; $i < count($accounts); $i++)
    	{
    		$sql = "select count(cta) as total from (
				select cta, descta, cuenta, sum(debe) as debe, sum(haber) as haber from( 
    				select rowid,cta,descta,subctade 
    				from (
    				    select * from ".MAIN_DB_PREFIX."contab_cat_ctas 
    				    order by subctade, rowid
    				) products_sorted, 
    				(select @pv := ".$accounts[$i].") initialisation 
					where (find_in_set(subctade, @pv) > 0 or rowid = ".$accounts[$i].") and @pv := concat(@pv, ',', rowid)) ctas 
    				inner join ".MAIN_DB_PREFIX."contab_polizasdet pdet on pdet.cuenta = ctas.cta 
					INNER JOIN ".MAIN_DB_PREFIX."contab_polizas as p on pdet.fk_poliza = p.rowid
					where p.fecha between '".$startdate."' and '".$enddate."'
				group by cta, descta, cuenta ) res ";
			$resql = $db->query($sql);
    		$row = $db->fetch_row($resql);
    		$json["total"] = intval($row[0]) + $json["total"];
    		
    		
    		$sql = "select *, 0 as saldo_inicial, (debe-haber) as saldo from (
        		select cuenta as rowid, cta, descta, cuenta, sum(debe) as debe, sum(haber) as haber, 'closed' as state from( 
    				select rowid,cta,descta,subctade 
    				from (
    				    select * from ".MAIN_DB_PREFIX."contab_cat_ctas 
    				    order by subctade, rowid
    				) products_sorted, 
    				(select @pv := ".$accounts[$i].") initialisation 
					where (find_in_set(subctade, @pv) > 0 or rowid = ".$accounts[$i].") and @pv := concat(@pv, ',', rowid)) ctas 
    				inner join ".MAIN_DB_PREFIX."contab_polizasdet pdet on pdet.cuenta = ctas.cta 
					INNER JOIN ".MAIN_DB_PREFIX."contab_polizas as p on pdet.fk_poliza = p.rowid
					where p.fecha between '".$startdate."' and '".$enddate."'
				group by cta, descta, cuenta ) result limit $offset,$rows";
    
	    	$resql = $db->query($sql);
	    	
	    	
			while($row = $db->fetch_object($resql)) 
			{
				$saldo_inicial = $ctas_functions->fetch_initial_mounts($row->rowid, $startdate);
				if($saldo_inicial["status"] == 1)
				{
					$row->saldo_inicial = $saldo_inicial["saldo"];
					$row->saldo = strval(floatval($row->saldo) + floatval($saldo_inicial["saldo"]));
				}
				array_push($data, $row);
			}
    	}
    	$json["rows"] = $data;
    	
    }

	
	echo json_encode($json);
