<?php
	header('Content-type:application/json;charset=utf-8');
	
	$res=0;
	// Try main.inc.php into web root known defined into CONTEXT_DOCUMENT_ROOT (not always defined)
	if (! $res && ! empty($_SERVER["CONTEXT_DOCUMENT_ROOT"])) $res=@include($_SERVER["CONTEXT_DOCUMENT_ROOT"]."/main.inc.php");
	// Try main.inc.php into web root detected using web root caluclated from SCRIPT_FILENAME
	$tmp=empty($_SERVER['SCRIPT_FILENAME'])?'':$_SERVER['SCRIPT_FILENAME'];$tmp2=realpath(__FILE__); $i=strlen($tmp)-1; $j=strlen($tmp2)-1;
	while($i > 0 && $j > 0 && isset($tmp[$i]) && isset($tmp2[$j]) && $tmp[$i]==$tmp2[$j]) { $i--; $j--; }
	if (! $res && $i > 0 && file_exists(substr($tmp, 0, ($i+1))."/main.inc.php")) $res=@include(substr($tmp, 0, ($i+1))."/main.inc.php");
	if (! $res && $i > 0 && file_exists(dirname(substr($tmp, 0, ($i+1)))."/main.inc.php")) $res=@include(dirname(substr($tmp, 0, ($i+1)))."/main.inc.php");
	// Try main.inc.php using relative path
	if (! $res && file_exists("../../main.inc.php")) $res=@include("../../main.inc.php");
	if (! $res && file_exists("../../../main.inc.php")) $res=@include("../../../main.inc.php");
	if (! $res) die("Include of main fails");

	if (file_exists(DOL_DOCUMENT_ROOT . '/reports/class/accounts.class.php')) {
		require_once DOL_DOCUMENT_ROOT . '/reports/class/accounts.class.php';
	}
	
	global $langs, $user, $conf, $db;

	$ctas_functions = new Accounts($db);
	
	$page = isset($_POST['page']) ? intval($_POST['page']) : 1;
	$rows = isset($_POST['rows']) ? intval($_POST['rows']) : 10;
	$offset = ($page-1)*$rows;

	
	$startdate = GETPOST('startdate');
    $enddate = GETPOST('enddate');
    $option = GETPOST('option');
    $id = GETPOST('id');

	$json = array();
	$data = array();
	
	if(isset($id) && $id > 0)
	{	
		echo json_encode($ctas_functions->fetch_accountsvalues_of_period($id, $startdate, $enddate));
	}else
	{
		if($option >= 1 && $option <= 3 )
		{
			if($option == 1)
				$ctas = $ctas_functions->fetch_activos_acount();
			
			if($option == 2)
				$ctas = $ctas_functions->fetch_pasivo_acount();
			
			if($option == 3)
			{
				$ctas = $ctas_functions->fetch_capital_acount();
				$edo_res = $ctas_functions->get_edo_results($startdate, $enddate);
			}
			
			$sql = "select ctas.rowid as id, ctas.cta, ctas.descta as name, ctas.subctade as parentId, sctas.natur, pv, 0 as saldo
						from( select rowid,cta,descta,subctade,fk_sat_cta, @pv as pv 
							from ( 
						        select * from ".MAIN_DB_PREFIX."contab_cat_ctas 
						        order by subctade, rowid 
						    ) products_sorted, 
						   	(select @pv := $ctas) initialisation 
						where (find_in_set(subctade, @pv) > 0 or rowid = $ctas) 
						     and @pv := (case when ROUND((LENGTH(@pv) - LENGTH( REPLACE (@pv,\",\",\"\") ) ) / LENGTH(\",\")) < 3 then concat(@pv, ',', rowid) else @pv end)
						) ctas
						inner join ".MAIN_DB_PREFIX."contab_sat_ctas as sctas on ctas.fk_sat_cta = sctas.rowid";
			
			$resql = $db->query($sql);
			$data_tmp = array();
			while($row = $db->fetch_object($resql)) 
			{
				if($option == 3 && $ctas == $row->id)
				{ 

					$saldo_capital = $ctas_functions->fetch_mounts_of_period_from_some_accounts($ctas, $startdate, $enddate);
					$row->saldo = floatval($saldo_capital) + floatval($edo_res);
					array_push($data_tmp, $row); 
				}else
				{
					if($row->id == $ctas_functions->fetch_edo_result_acount())
					{
						$value = $ctas_functions->fetch_mounts_of_period_from_some_accounts($row->id, $startdate, $enddate);
						if($value != 0)
						{
							$row->saldo = $edo_res + $value;
							array_push($data_tmp, $row);
						}else
						{
							$row->saldo = $edo_res;
							$row->state = "document";
							array_push($data_tmp, $row);
						}
					}else
					{
						$value = $ctas_functions->fetch_mounts_of_period_from_some_accounts($row->id, $startdate, $enddate);
						$row->saldo = strval($value);
						if($ctas == $row->id || floatval($row->saldo) != 0)
							array_push($data_tmp, $row);
					}
				}
			}
			
			echo json_encode($data_tmp);
		}
	}
	
