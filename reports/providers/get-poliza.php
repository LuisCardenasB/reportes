<?php
	header('Content-type:application/json;charset=utf-8');
	
	//SECURING AREA
	$res=0;
	// Try main.inc.php into web root known defined into CONTEXT_DOCUMENT_ROOT (not always defined)
	if (! $res && ! empty($_SERVER["CONTEXT_DOCUMENT_ROOT"])) $res=@include($_SERVER["CONTEXT_DOCUMENT_ROOT"]."/main.inc.php");
	// Try main.inc.php into web root detected using web root caluclated from SCRIPT_FILENAME
	$tmp=empty($_SERVER['SCRIPT_FILENAME'])?'':$_SERVER['SCRIPT_FILENAME'];$tmp2=realpath(__FILE__); $i=strlen($tmp)-1; $j=strlen($tmp2)-1;
	while($i > 0 && $j > 0 && isset($tmp[$i]) && isset($tmp2[$j]) && $tmp[$i]==$tmp2[$j]) { $i--; $j--; }
	if (! $res && $i > 0 && file_exists(substr($tmp, 0, ($i+1))."/main.inc.php")) $res=@include(substr($tmp, 0, ($i+1))."/main.inc.php");
	if (! $res && $i > 0 && file_exists(dirname(substr($tmp, 0, ($i+1)))."/main.inc.php")) $res=@include(dirname(substr($tmp, 0, ($i+1)))."/main.inc.php");
	// Try main.inc.php using relative path
	if (! $res && file_exists("../../main.inc.php")) $res=@include("../../main.inc.php");
	if (! $res && file_exists("../../../main.inc.php")) $res=@include("../../../main.inc.php");
	if (! $res) die("Include of main fails");

	//FRAMEWORK DB ACCESS
	global $langs, $user, $conf, $db;
	
	//PAGINATION
	$page = isset($_POST['page']) ? intval($_POST['page']) : 1;
	$rows = isset($_POST['rows']) ? intval($_POST['rows']) : 10;
	$offset = ($page-1)*$rows;
	
	$sql = "SELECT
			t.tipo_pol,
			t.anio,
			t.mes,
			t.concepto,
			t.fecha,
			";
        
        //AVOID ISSUES WITH ESPECIAL CHARS
    	$db->query('SET NAMES utf8;');
    	
    	$resql = $db->query($sql);
    	
    	//DATA ARRAY
    	$data = array();
    	
    	//RECURSIVE COUNT TOTALS
    	$count_amount = 0;
    	$count_tva = 0;
    	$count_ttc = 0;
		while($row = $db->fetch_object($resql)) 
		{
			
			array_push($data, $row);
			$count_amount += $row->amount;
			$count_tva += $row->total_tva;
			$count_ttc += $row->total_ttc;
		}
		
		//CREATE FOOTER ARRAY
		$footer = array([
			'famount' => number_format($count_amount,2),
			'ftotal_tva' => number_format($count_tva,2),
			'ftotal_ttc' => number_format($count_ttc,2),
			'project_title' => 'TOTAL:',
			]);
		
		//CREATE TOTAL ARRAY WITH COUNT ELEMENTS IN $data ARRAY
		$json["total"] = count($data);
		
    	$json["rows"] = $data;
    	$json["footer"] = $footer;
    	
    	echo json_encode($json);