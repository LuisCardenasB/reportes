<?php
	header('Content-type:application/json;charset=utf-8');
	
	$res=0;
	// Try main.inc.php into web root known defined into CONTEXT_DOCUMENT_ROOT (not always defined)
	if (! $res && ! empty($_SERVER["CONTEXT_DOCUMENT_ROOT"])) $res=@include($_SERVER["CONTEXT_DOCUMENT_ROOT"]."/main.inc.php");
	// Try main.inc.php into web root detected using web root caluclated from SCRIPT_FILENAME
	$tmp=empty($_SERVER['SCRIPT_FILENAME'])?'':$_SERVER['SCRIPT_FILENAME'];$tmp2=realpath(__FILE__); $i=strlen($tmp)-1; $j=strlen($tmp2)-1;
	while($i > 0 && $j > 0 && isset($tmp[$i]) && isset($tmp2[$j]) && $tmp[$i]==$tmp2[$j]) { $i--; $j--; }
	if (! $res && $i > 0 && file_exists(substr($tmp, 0, ($i+1))."/main.inc.php")) $res=@include(substr($tmp, 0, ($i+1))."/main.inc.php");
	if (! $res && $i > 0 && file_exists(dirname(substr($tmp, 0, ($i+1)))."/main.inc.php")) $res=@include(dirname(substr($tmp, 0, ($i+1)))."/main.inc.php");
	// Try main.inc.php using relative path
	if (! $res && file_exists("../../main.inc.php")) $res=@include("../../main.inc.php");
	if (! $res && file_exists("../../../main.inc.php")) $res=@include("../../../main.inc.php");
	if (! $res) die("Include of main fails");

	
	
	global $langs, $user, $conf, $db;
	
	$q = GETPOST('q');
	$option = GETPOST('option');
	$filter = "";
	$json = array();
	
	if(isset($option) && $option == 1)
	{
		if(isset($q))
		{
			$filter = " where text like '%$q%' ";
			
		}
		
		$sql = 'SELECT * FROM (SELECT rowid as id, concat(cta, " - ", descta) as text, descta, cta from '.MAIN_DB_PREFIX.'contab_cat_ctas ) result '.$filter;
		$resql = $db->query($sql);
		while($row = $db->fetch_object($resql)) 
		{
			array_push($json, $row);
		}
		
	}
	
	if(isset($option) && $option == 2)
	{
		$start_account = GETPOST('start_select');
		$account_filter = "";
		if(isset($start_account) && $start_account != "")
		{
			//Get parent
			$sql = "select subctade from ".MAIN_DB_PREFIX."contab_cat_ctas where rowid = '" . $start_account . "' ";
			$resql = $db->query($sql);
			$subctade = "";
			while($row = $db->fetch_object($resql)) 
			{
				$subctade = " and subctade = " . $row->subctade;
			}
			
			$account_filter = ' where cta > (SELECT cta FROM '.MAIN_DB_PREFIX.'contab_cat_ctas where rowid = ' . $start_account . ' ) ' . $subctade . ' ';

			if(isset($q))
				$filter = " where text like '%$q%' ";
			
			$sql = 'SELECT * FROM (SELECT rowid as id, concat(cta, " - ", descta) as text, descta, cta from '.MAIN_DB_PREFIX.'contab_cat_ctas ' . $account_filter . ' ) result '.$filter;
			$resql = $db->query($sql);
			while($row = $db->fetch_object($resql)) 
			{
				array_push($json, $row);
			}
		}
		
	}
	
	echo json_encode($json);
