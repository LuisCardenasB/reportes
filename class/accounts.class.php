<?php
/* Copyright (C) 2017  Laurent Destailleur <eldy@users.sourceforge.net>
 * Copyright (C) ---Put here your own copyright and developer email---
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

/**
 * \file        htdocs/modulebuilder/template/class/myobject.class.php
 * \ingroup     mymodule
 * \brief       This file is a CRUD class file for MyObject (Create/Read/Update/Delete)
 */

// Put here all includes required by your class file
require_once DOL_DOCUMENT_ROOT . '/core/class/commonobject.class.php';
//require_once DOL_DOCUMENT_ROOT . '/societe/class/societe.class.php';
//require_once DOL_DOCUMENT_ROOT . '/product/class/product.class.php';

/**
 * Class for MyObject
 */
class Accounts extends CommonObject
{
	/**
	 * @var string ID to identify managed object
	 */
	public $element = 'accounts';


	/**
	 * Constructor
	 *
	 * @param DoliDb $db Database handler
	 */
	public function __construct(DoliDB $db)
	{
		global $conf;

		$this->db = $db;

		if (empty($conf->global->MAIN_SHOW_TECHNICAL_ID)) $this->fields['rowid']['visible']=0;
		if (empty($conf->multicompany->enabled)) $this->fields['entity']['enabled']=0;
	}

	/**
	 * Create object into database
	 *
	 * @param  User $user      User that creates
	 * @param  bool $notrigger false=launch triggers after, true=disable triggers
	 * @return int             <0 if KO, Id of created object if OK
	 */
	public function fetch_initial_mounts($id, $until_date)
	{
		global $langs,$conf;
    	$sql = "select ifnull((SUM(debe) - SUM(haber)),0) as saldo from(
				    select rowid, fecha, tipo_pol, cons, descta, sum(debe) as debe, sum(haber) as haber from (
				        SELECT p.rowid as rowid, p.fecha, p.tipo_pol, p.cons, p.concepto as descta, det.debe, det.haber 
				        FROM ".MAIN_DB_PREFIX."contab_polizasdet as det
				        INNER JOIN ".MAIN_DB_PREFIX."contab_polizas as p on det.fk_poliza = p.rowid
				        INNER JOIN ".MAIN_DB_PREFIX."contab_cat_ctas as cta on det.cuenta = cta.cta
				    where p.fecha < '$until_date 00:00:00.000' and cta = '$id'
				order by fecha ) result group by rowid, fecha, tipo_pol, cons, descta ) result";
    	
    	dol_syslog(get_class($this)."::fetch_initial_mounts sql=".$sql, LOG_DEBUG);
    	$resql=$this->db->query($sql);
    	if ($resql)
    	{
    		if ($this->db->num_rows($resql))
    		{
    			$obj = $this->db->fetch_object($resql);
    			$res = array("status" => 1, "saldo" => $obj->saldo);
    			$this->db->free($resql);
    			return $res;
    
    		} else {
    			$this->db->free($resql);
    			return array("status" => 0, "saldo" => 0);
    		}
    	}
    	else
    	{
    		$this->error="Error ".$this->db->lasterror();
    		dol_syslog(get_class($this)."::fetch_initial_mounts ".$this->error, LOG_ERR);
    		return array("status" => -1, "saldo" => 0);
    	}
	}
	
	public function fetch_initial_tree_mounts($id, $until_date)
	{
		global $langs,$conf;
		$sql = "select ifnull(saldo, 0) as saldo from 
				( select 
                 	sum((case when natur = 'A' then ((debe-haber) * -1) else (debe-haber) end)) as saldo 
                from( select rowid,cta,descta,subctade, natur 
					from ( select c.*, sc.natur
                      from ".MAIN_DB_PREFIX."contab_cat_ctas as c 
                      inner join ".MAIN_DB_PREFIX."contab_sat_ctas as sc on c.fk_sat_cta = sc.rowid 
                	order by subctade, rowid ) products_sorted, 
				(select @pv := $id) initialisation 
				where (find_in_set(subctade, @pv) > 0 or rowid = $id) and @pv := concat(@pv, ',', rowid)) ctas 
				inner join ".MAIN_DB_PREFIX."contab_polizasdet pdet on pdet.cuenta = ctas.cta 
				INNER JOIN ".MAIN_DB_PREFIX."contab_polizas as p on pdet.fk_poliza = p.rowid 
                where p.fecha < '$until_date 00:00:00.000' ) result";
    	
    	dol_syslog(get_class($this)."::fetch_mounts_of_period sql=".$sql, LOG_DEBUG);
    	$resql=$this->db->query($sql);
    	if ($resql)
    	{
    		if ($this->db->num_rows($resql))
    		{
    			$obj = $this->db->fetch_object($resql);
    			$res = array("status" => 1, "saldo" => $obj->saldo);
    			$this->db->free($resql);
    			return $res;
    
    		} else {
    			$this->db->free($resql);
    			return array("status" => 0, "saldo" => 0);
    		}
    	}
    	else
    	{
    		$this->error="Error ".$this->db->lasterror();
    		dol_syslog(get_class($this)."::fetch_mounts_of_period ".$this->error, LOG_ERR);
    		return array("status" => -1, "saldo" => 0);
    	}
	}
	
	public function fetch_mounts_of_period($id, $startdate, $enddate)
	{
		global $langs,$conf;
		$sql = "select ifnull(saldo, 0) as saldo from 
				( select 
                 	sum((case when natur = 'A' then ((debe-haber) * -1) else (debe-haber) end)) as saldo 
                from( select rowid,cta,descta,subctade, natur 
					from ( select c.*, sc.natur
                      from ".MAIN_DB_PREFIX."contab_cat_ctas as c 
                      inner join ".MAIN_DB_PREFIX."contab_sat_ctas as sc on c.fk_sat_cta = sc.rowid 
                	order by subctade, rowid ) products_sorted, 
				(select @pv := $id) initialisation 
				where (find_in_set(subctade, @pv) > 0 or rowid = $id) and @pv := concat(@pv, ',', rowid)) ctas 
				inner join ".MAIN_DB_PREFIX."contab_polizasdet pdet on pdet.cuenta = ctas.cta 
				INNER JOIN ".MAIN_DB_PREFIX."contab_polizas as p on pdet.fk_poliza = p.rowid 
                where p.fecha between '$startdate 00:00:00.000' and '$enddate 23:59:59.999' ) result";
    	
    	dol_syslog(get_class($this)."::fetch_mounts_of_period sql=".$sql, LOG_DEBUG);
    	$resql=$this->db->query($sql);
    	if ($resql)
    	{
    		if ($this->db->num_rows($resql))
    		{
    			$obj = $this->db->fetch_object($resql);
    			$res = array("status" => 1, "saldo" => $obj->saldo);
    			$this->db->free($resql);
    			return $res;
    
    		} else {
    			$this->db->free($resql);
    			return array("status" => 0, "saldo" => 0);
    		}
    	}
    	else
    	{
    		$this->error="Error ".$this->db->lasterror();
    		dol_syslog(get_class($this)."::fetch_mounts_of_period ".$this->error, LOG_ERR);
    		return array("status" => -1, "saldo" => 0);
    	}
	}
	
	public function fetch_accountsvalues_of_period($id, $startdate, $enddate)
	{
		global $langs,$conf;
		$sql = "select ctas.rowid as id, cta, descta as name, $id as parentId,
                 	sum((case when natur = 'A' then ((debe-haber) * -1) else (debe-haber) end)) as saldo 
                from( select rowid,cta,descta,subctade,natur 
					from ( select c.*, sc.natur
                      from ".MAIN_DB_PREFIX."contab_cat_ctas as c 
                      inner join ".MAIN_DB_PREFIX."contab_sat_ctas as sc on c.fk_sat_cta = sc.rowid 
                	order by subctade, rowid ) products_sorted, 
				(select @pv := $id) initialisation 
				where (find_in_set(subctade, @pv) > 0 or rowid = $id) and @pv := concat(@pv, ',', rowid)) ctas 
				inner join ".MAIN_DB_PREFIX."contab_polizasdet pdet on pdet.cuenta = ctas.cta 
				INNER JOIN ".MAIN_DB_PREFIX."contab_polizas as p on pdet.fk_poliza = p.rowid 
                where p.fecha between '$startdate 00:00:00.000' and '$enddate 23:59:59.999'
                group by ctas.rowid, cta, descta";
    	
    	dol_syslog(get_class($this)."::fetch_accountsvalues_of_period sql=".$sql, LOG_DEBUG);
    	$resql=$this->db->query($sql);
    	$data = array();
    	while($row = $this->db->fetch_object($resql)) 
    	{
			array_push($data, $row);
    	}
    	if($id == $this->fetch_edo_result_acount())
    	{
    		array_push($data, 
    			array(
    				'id' => 'calcidedores', 
    				'name' => 'Estado de Resultados', 
    				'parentId' => $id,
    				'saldo' => $this->get_edo_results($startdate, $enddate)
    			));
    	}
    	
    	$this->db->free($resql);
    	
    	return $data;
	}
	
	public function fetch_ventas_acount()
	{
		global $langs,$conf;
		
		//Create a page where decide what account you are going to use to get Ventas
		//Return id cta
		$account = 443;
		
		$sql = "SELECT rowid FROM `".MAIN_DB_PREFIX."contab_cat_ctas` where fk_sat_cta = $account";
		$rs = $this->db->query($sql);
		$final_account = 443;
		while($row = $this->db->fetch_array($rs)) { $final_account = $row["rowid"]; }
		
		return $final_account;
	}
	
	public function fetch_costos_acounts()
	{
		global $langs,$conf;
		
		//Create a page where decide what account you are going to use to get Ventas
		//Return id cta
		$accounts = array(494, 503, 508, 510, 536);
		$sql = "Select rowid from ".MAIN_DB_PREFIX."contab_cat_ctas where fk_sat_cta in (" . implode (", ", $accounts) . ")";
		$rs = $this->db->query($sql);
		
		$final_accounts = array();
		while($row = $this->db->fetch_array($rs)) {	array_push($final_accounts, strval($row["rowid"]));	}
		
		return $final_accounts;
	}
	
	public function fetch_gastos_acounts()
	{
		global $langs,$conf;
		
		//Create a page where decide what account you are going to use to get Ventas
		//Return id cta
		$accounts = array(540, 625, 710, 793, 876, 908, 910, 912, 914);
		$sql = "Select rowid from ".MAIN_DB_PREFIX."contab_cat_ctas where fk_sat_cta in (" . implode (", ", $accounts) . ")";
		$rs = $this->db->query($sql);
		
		$final_accounts = array();
		while($row = $this->db->fetch_array($rs)) {	array_push($final_accounts, strval($row["rowid"]));	}
		
		return $final_accounts;
	}
	
	public function fetch_gastos_acount()
	{
		global $langs,$conf;
		
		//Create a page where decide what account you are going to use to get Ventas
		//Return id cta
		$account = 953;
		
		$sql = "SELECT rowid FROM `".MAIN_DB_PREFIX."contab_cat_ctas` where fk_sat_cta = $account";
		$rs = $this->db->query($sql);
		$final_account = 953;
		while($row = $this->db->fetch_array($rs)) { $final_account = $row["rowid"]; }
		
		return $final_account;
	}
	
	public function fetch_edo_result_acount()
	{
		global $langs,$conf;
		
		//Create a page where decide what account you are going to use to get Ventas
		//Return id cta
		$account = 437;
		
		$sql = "SELECT rowid FROM `".MAIN_DB_PREFIX."contab_cat_ctas` where fk_sat_cta = $account";
		$rs = $this->db->query($sql);
		$final_account = 437;
		while($row = $this->db->fetch_array($rs)) { $final_account = $row["rowid"]; }
		
		return $final_account;
	}
	
	
	
	/** BALANCE ***/
	public function fetch_activos_acount()
	{
		global $langs,$conf;
		
		//Create a page where decide what account you are going to use to get Ventas
		//Return id cta
		$account = 1;
		
		$sql = "SELECT rowid FROM `".MAIN_DB_PREFIX."contab_cat_ctas` where fk_sat_cta = $account";
		$rs = $this->db->query($sql);
		$final_account = 1;
		while($row = $this->db->fetch_array($rs)) { $final_account = $row["rowid"]; }
		
		return $final_account;
	}
	
	public function fetch_pasivo_acount()
	{
		global $langs,$conf;
		
		//Create a page where decide what account you are going to use to get Ventas
		//Return id cta
		$account = 250;
		
		$sql = "SELECT rowid FROM `".MAIN_DB_PREFIX."contab_cat_ctas` where fk_sat_cta = $account";
		$rs = $this->db->query($sql);
		$final_account = 250;
		while($row = $this->db->fetch_array($rs)) { $final_account = $row["rowid"]; }
		
		return $final_account;
	}
	
	public function fetch_capital_acount()
	{
		global $langs,$conf;
		
		//Create a page where decide what account you are going to use to get Ventas
		//Return id cta
		$account = 419;
		
		$sql = "SELECT rowid FROM `".MAIN_DB_PREFIX."contab_cat_ctas` where fk_sat_cta = $account";
		$rs = $this->db->query($sql);
		$final_account = 419;
		while($row = $this->db->fetch_array($rs)) { $final_account = $row["rowid"]; }
		
		return $final_account;
	}
	
	public function get_applicable_taxes()
	{
		return array
		(  
			array( "id" => 1, "name" => "ISR 30%", "value" => 0.3 ),
			array( "id" => 2, "name" => "PTU 10%", "value" => 0.1 )
		);
	}
	
	public function get_edo_results($startdate, $enddate)
	{
		$ventas = $this->fetch_mounts_of_period_from_some_accounts($this->fetch_ventas_acount(), $startdate, $enddate);
		$costos = $this->fetch_mounts_of_period_from_some_accounts($this->fetch_costos_acounts(), $startdate, $enddate);
		$gastos = $this->fetch_mounts_of_period_from_some_accounts($this->fetch_gastos_acounts(), $startdate, $enddate);
		$gastos_fin = $this->fetch_mounts_of_period_from_some_accounts($this->fetch_gastos_acount(), $startdate, $enddate);
		
		if($gastos_fin < 0)
			$gastos_fin = abs($gastos_fin);
		
		$utilidad_antes_impuestos = ($ventas - ($costos + $gastos)) - $gastos_fin; 
		$utilidad_neta = $utilidad_antes_impuestos;
		$impuestos = $this->get_applicable_taxes();
		
		for($i = 0; $i < count($impuestos); $i++ )
		{
			$utilidad_neta = $utilidad_neta - ( $utilidad_antes_impuestos * $impuestos[$i]["value"] );
		}
		
		return $utilidad_neta; 
	}
	
	public function get_activo_results($startdate, $enddate)
	{
		return $this->fetch_mounts_of_period_from_some_accounts($this->fetch_activos_acount(), $startdate, $enddate);
	}
	
	public function get_pasivo_capital_results($startdate, $enddate)
	{
		$pasivo = $this->fetch_mounts_of_period_from_some_accounts($this->fetch_pasivo_acount(), $startdate, $enddate);
		$capital = $this->fetch_mounts_of_period_from_some_accounts($this->fetch_capital_acount(), $startdate, $enddate);
		$edo_res = $this->get_edo_results($startdate, $enddate);
		
		return $pasivo + ($capital + $edo_res);
	}
	
	
	
	public function fetch_mounts_of_period_from_some_accounts($accounts, $startdate, $enddate)
	{
		global $langs,$conf;
		if(is_array($accounts))
			$ctas = implode (",", $accounts);
		else
			$ctas = $accounts;
		
		$sql = "select ifnull( sum((case when natur = 'A' then ((debe-haber) * -1) else (debe-haber) end)), 0) as saldo
					from(
					    select ctas.rowid, ctas.cta, ctas.descta, ctas.subctade, sctas.natur, debe, haber
					    from( select rowid,cta,descta,subctade,fk_sat_cta 
					        from ( 
					            select * from ".MAIN_DB_PREFIX."contab_cat_ctas 
					            order by subctade, rowid 
					        ) products_sorted, 
					        (select @pv := '$ctas') initialisation 
					    where (find_in_set(subctade, @pv) > 0) 
					         and @pv := concat(@pv, ',', rowid)
					    ) ctas
					    inner join ".MAIN_DB_PREFIX."contab_sat_ctas as sctas on ctas.fk_sat_cta = sctas.rowid
					    inner join ".MAIN_DB_PREFIX."contab_polizasdet pdet on pdet.cuenta = ctas.cta 
					    INNER JOIN ".MAIN_DB_PREFIX."contab_polizas as p on pdet.fk_poliza = p.rowid 
					    where p.fecha between '$startdate 00:00:00.000' and '$enddate 23:59:59.999' 
					) result";
    	
    	dol_syslog(get_class($this)."::fetch_mounts_of_period_from_some_accounts sql=".$sql, LOG_DEBUG);
    	$resql=$this->db->query($sql);
    	if ($resql)
    	{
    		if ($this->db->num_rows($resql))
    		{
    			$obj = $this->db->fetch_object($resql);
    			$this->db->free($resql);
    			return $obj->saldo;
    
    		} else {
    			$this->db->free($resql);
    			return 0;
    		}
    	}
    	else
    	{
    		$this->error="Error ".$this->db->lasterror();
    		dol_syslog(get_class($this)."::ffetch_mounts_of_period_from_some_accounts ".$this->error, LOG_ERR);
    		return 0;
    	}
	}
	
	public function getNumberFormat($number)
	{
		if(floatval($number) < 0)
		{
			$res = number_format((floatval($number) * -1), 2);
			$res = "$ ( " . $res . " )";
		}
		else
		{
			$res = number_format(floatval($number), 2);
			$res = "$ " . $res;
		}
		
		return $res;
	}
	
	public function getClassColor($number)
	{
		if( floatval($number) < 0 ) 
			return "class='negative-money center'"; 
		else 
			return "";
	}
	
}
