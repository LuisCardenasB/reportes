<?php
/* Copyright (C) 2017  Laurent Destailleur <eldy@users.sourceforge.net>
 * Copyright (C) ---Put here your own copyright and developer email---
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

/**
 * \file        htdocs/modulebuilder/template/class/myobject.class.php
 * \ingroup     mymodule
 * \brief       This file is a CRUD class file for MyObject (Create/Read/Update/Delete)
 */

// Put here all includes required by your class file
require_once DOL_DOCUMENT_ROOT . '/core/class/commonobject.class.php';
//require_once DOL_DOCUMENT_ROOT . '/societe/class/societe.class.php';
//require_once DOL_DOCUMENT_ROOT . '/product/class/product.class.php';

/**
 * Class for MyObject
 */
class Utils extends CommonObject
{
	/**
	 * @var string ID to identify managed object
	 */
	public $element = 'utils';


	/**
	 * Constructor
	 *
	 * @param DoliDb $db Database handler
	 */
	public function __construct(DoliDB $db)
	{
		global $conf;

		$this->db = $db;

		if (empty($conf->global->MAIN_SHOW_TECHNICAL_ID)) $this->fields['rowid']['visible']=0;
		if (empty($conf->multicompany->enabled)) $this->fields['entity']['enabled']=0;
	}

	/**
	 * Create object into database
	 *
	 * @param  User $user      User that creates
	 * @param  bool $notrigger false=launch triggers after, true=disable triggers
	 * @return int             <0 if KO, Id of created object if OK
	 */
	public function exists($rows, $parentId)
	{
		for($i = 0; $i < count($rows); $i++)
		{
            if ($rows[$i]->id == $parentId) return true;
        }
        return false;
	}
	
	public function convert($rows)
	{
		$nodes = array();
		$row = $rows;
        for($i = 0; $i < count($rows); $i++){
	        $row = $rows[$i];
	        if (!$this->exists($rows, $row->parentId)){
	            array_push($nodes, array("id" => $row->id, "descta" => $row->descta, 
	            						"cta" => $row->cta, "saldo" => $row->saldo, "parentId" => $row->parentId));
	        }
	    }
	    
	    for($i = 0; $i < count($nodes); $i++)
	    {
	    	$root = $nodes[$i];
	    	$child_l1 = array();
	        for($j = 0; $j < count($rows); $j++)
	        {
	        	$row_level1 = $rows[$j];
	        	if($root->id == $row_level1->parentId)
	        	{
	        		for($k = 0; $k < count($rows); $k++)
			        {
			        	
			        }
			        array_push();
	        	}
	        	
	        }
	    }
	    
	    
	    /*
	    $toDo = array();
	    for($i = 0; $i < count($nodes); $i++){
	        array_push($toDo, $nodes[$i]);
	    }
	    while(count($toDo)){
	        $node = array_shift($toDo);    // the parent node
	        // get the children nodes
	        for($i = 0; $i < count(rows); $i++){
	            $row = $rows[$i];
	            if ($row->parentId == $node->id){
	                $child = array
	                	(
	                		"id" => $row->id,
	                		"descta" => $row->name,
	                		"cta" => $row->cta, 
	                		"saldo" => $row->saldo
	                	); 
	                	
	                if ($node->children){
	                    array_push($node->children, $child);
	                } else {
	                    $node->children = [$child];
	                }
	                array_push($toDo, $child);
	            }
	        }
	    } */
	    
	    return json_encode($nodes);
	}
	
	
}
