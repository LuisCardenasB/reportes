<?php


// Load Dolibarr environment
$res=0;
// Try main.inc.php into web root known defined into CONTEXT_DOCUMENT_ROOT (not always defined)
if (! $res && ! empty($_SERVER["CONTEXT_DOCUMENT_ROOT"])) $res=@include($_SERVER["CONTEXT_DOCUMENT_ROOT"]."/main.inc.php");
// Try main.inc.php into web root detected using web root caluclated from SCRIPT_FILENAME
$tmp=empty($_SERVER['SCRIPT_FILENAME'])?'':$_SERVER['SCRIPT_FILENAME'];$tmp2=realpath(__FILE__); $i=strlen($tmp)-1; $j=strlen($tmp2)-1;
while($i > 0 && $j > 0 && isset($tmp[$i]) && isset($tmp2[$j]) && $tmp[$i]==$tmp2[$j]) { $i--; $j--; }
if (! $res && $i > 0 && file_exists(substr($tmp, 0, ($i+1))."/main.inc.php")) $res=@include(substr($tmp, 0, ($i+1))."/main.inc.php");
if (! $res && $i > 0 && file_exists(dirname(substr($tmp, 0, ($i+1)))."/main.inc.php")) $res=@include(dirname(substr($tmp, 0, ($i+1)))."/main.inc.php");
// Try main.inc.php using relative path
if (! $res && file_exists("../../main.inc.php")) $res=@include("../../main.inc.php");
if (! $res && file_exists("../../../main.inc.php")) $res=@include("../../../main.inc.php");
if (! $res) die("Include of main fails");

//Custom Class
if (file_exists(DOL_DOCUMENT_ROOT . '/reports/class/accounts.class.php')) {
	require_once DOL_DOCUMENT_ROOT . '/reports/class/accounts.class.php';
}

require 'vendor/autoload.php';

use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;

global $langs, $user, $conf, $db;

	$ctas_functions = new Accounts($db);

	$startdate = GETPOST('startdate');
	$enddate = GETPOST('enddate');

	
    $spreadsheet = new Spreadsheet();
	$sheet = $spreadsheet->getActiveSheet();
	$sheet->setCellValue('A1', 'REPORTE DE ESTADO DE RESULTADOS');
	$sheet->mergeCells('A1:D1');
	$sheet->getStyle('A1:D1')->getAlignment()->setHorizontal('center');
	$sheet->getStyle('A1:D1')->getFont()->setBold(true);
	
	$sheet->setCellValue('A2', "MOVIMIENTOS DEL $startdate AL $enddate");
	$sheet->mergeCells('A2:D2');
	$sheet->getStyle('A2:D2')->getAlignment()->setHorizontal('center');
	$sheet->getStyle('A2:D2')->getFont()->setBold(true);
	
	$sheet->setCellValue('A3', "MONEDA: PESO MEXICANO");
	$sheet->mergeCells('A3:D3');
	$sheet->getStyle('A3:D3')->getAlignment()->setHorizontal('center');
	$sheet->getStyle('A3:D3')->getFont()->setBold(true);
    $sheet->mergeCells('A4:D4');
    //Headers
    $sheet->setCellValue('A5', "Cuenta");
    $sheet->setCellValue('B5', "Nombre");
    $sheet->mergeCells('B5:C5');
    $sheet->setCellValue('D5', "Saldos");
    $sheet->getStyle('A5:D5')->getFont()->setBold(true);
    $sheet->getStyle('A5')->getAlignment()->setHorizontal('center');
    $sheet->getStyle('D5')->getAlignment()->setVertical('center');
    
    $row_number = 6;
    
    $ventas = 0;
	$cta_venta = $ctas_functions->fetch_ventas_acount();
	$sql = "SELECT cc.rowid, cc.cta, cc.descta, 0 as saldo, 'closed' as state FROM ".MAIN_DB_PREFIX."contab_cat_ctas as cc
				inner join ".MAIN_DB_PREFIX."contab_sat_ctas sc on cc.fk_sat_cta = sc.rowid
				where cc.subctade = $cta_venta";
    
    $rownumber_tmp = $row_number;
    $row_number++;
    
    $resql = $db->query($sql);
	$data_tmp = array();
	while($row = $db->fetch_object($resql)) 
	{
		$value = $ctas_functions->fetch_mounts_of_period($row->rowid, $startdate, $enddate);
		if($value["status"] == 1)
		{
			$row->saldo = strval($value["saldo"]);
			$ventas = $ventas + floatval($value["saldo"]);
			
			$sheet->setCellValue("A$row_number", $row->cta);
			$sheet->setCellValue("C$row_number", $row->descta);
			$sheet->setCellValue("D$row_number", floatval($value["saldo"]));
			$sheet->getStyle("D$row_number")->getNumberFormat()->setFormatCode('"$"* #,##0.00_-;_("$"* \(#,##0.00\);_("$"* "0.00"_);_(@_)');
			if(floatval($value["saldo"]) < 0)
				$sheet->getStyle("D$row_number")->getFont()->getColor()->setARGB('FF0000');
			
			$sheet->getStyle("A$row_number")->getAlignment()->setHorizontal('center');
			$sheet->getStyle("D$row_number")->getAlignment()->setHorizontal('center');
			$row_number++;
			
		}
		
	}
	
	$sheet->setCellValue("A$rownumber_tmp", "");
	$sheet->setCellValue("B$rownumber_tmp", "INGRESOS POR VENTAS");
	$sheet->setCellValue("D$rownumber_tmp", $ventas);
	$sheet->getStyle("A$rownumber_tmp")->getAlignment()->setHorizontal('center');
	$sheet->getStyle("D$rownumber_tmp")->getAlignment()->setHorizontal('center');
	$sheet->getStyle("A$rownumber_tmp:H$rownumber_tmp")->getFont()->setBold(true);
	$sheet->getStyle("D$rownumber_tmp")->getNumberFormat()->setFormatCode('"$"* #,##0.00_-;_("$"* \(#,##0.00\);_("$"* "0.00"_);_(@_)');
	if($ventas < 0)
		$sheet->getStyle("D$rownumber_tmp")->getFont()->getColor()->setARGB('FF0000');
	
	$row_number++;
	
	
	
	$costos = 0;
	$cta_costos = $ctas_functions->fetch_costos_acounts();
	$sql = "SELECT cc.rowid, cc.cta, cc.descta, 0 as saldo, 'closed' as state FROM ".MAIN_DB_PREFIX."contab_cat_ctas as cc
			inner join ".MAIN_DB_PREFIX."contab_sat_ctas sc on cc.fk_sat_cta = sc.rowid
			where cc.rowid in (" . implode (", ", $cta_costos) . ")";
	
	$rownumber_tmp = $row_number;
    $row_number++;		
	
	$resql = $db->query($sql);
	$data_tmp = array();
	
	while($row = $db->fetch_object($resql)) 
	{
		$value = $ctas_functions->fetch_mounts_of_period($row->rowid, $startdate, $enddate);
		if($value["status"] == 1)
		{
			$row->saldo = strval($value["saldo"]);
			$costos = $costos + floatval($value["saldo"]);
			
			$sheet->setCellValue("A$row_number", $row->cta);
			$sheet->setCellValue("C$row_number", $row->descta);
			$sheet->setCellValue("D$row_number", floatval($value["saldo"]));
			$sheet->getStyle("D$row_number")->getNumberFormat()->setFormatCode('"$"* #,##0.00_-;_("$"* \(#,##0.00\);_("$"* "0.00"_);_(@_)');
			if(floatval($value["saldo"]) < 0)
				$sheet->getStyle("D$row_number")->getFont()->getColor()->setARGB('FF0000');
			
			$sheet->getStyle("A$row_number")->getAlignment()->setHorizontal('center');
			$sheet->getStyle("D$row_number")->getAlignment()->setHorizontal('center');
			$row_number++;
		}
		
	}
	
	$sheet->setCellValue("A$rownumber_tmp", "");
	$sheet->setCellValue("B$rownumber_tmp", "COSTOS DE VENTA");
	$sheet->setCellValue("D$rownumber_tmp", $costos);
	$sheet->getStyle("A$rownumber_tmp")->getAlignment()->setHorizontal('center');
	$sheet->getStyle("D$rownumber_tmp")->getAlignment()->setHorizontal('center');
	$sheet->getStyle("A$rownumber_tmp:H$rownumber_tmp")->getFont()->setBold(true);
	$sheet->getStyle("D$rownumber_tmp")->getNumberFormat()->setFormatCode('"$"* #,##0.00_-;_("$"* \(#,##0.00\);_("$"* "0.00"_);_(@_)');
	if($costos < 0)
		$sheet->getStyle("D$rownumber_tmp")->getFont()->getColor()->setARGB('FF0000');
	
	$row_number++;
	
	$ut_bruta_calc = $ventas - $costos;
	$text_ut_bruta_calc = "UTILIDAD BRUTA";
	if($ut_bruta_calc < 0)
		$text_ut_bruta_calc = "PERDIDA BRUTA";
			
	$rownumber_tmp = $row_number;
    $row_number++;
    $sheet->setCellValue("A$rownumber_tmp", "");
	$sheet->setCellValue("B$rownumber_tmp", $text_ut_bruta_calc);
	$sheet->setCellValue("D$rownumber_tmp", $ut_bruta_calc);
	$sheet->getStyle("A$rownumber_tmp")->getAlignment()->setHorizontal('center');
	$sheet->getStyle("D$rownumber_tmp")->getAlignment()->setHorizontal('center');
	$sheet->getStyle("A$rownumber_tmp:H$rownumber_tmp")->getFont()->setBold(true);
	$sheet->getStyle("D$rownumber_tmp")->getNumberFormat()->setFormatCode('"$"* #,##0.00_-;_("$"* \(#,##0.00\);_("$"* "0.00"_);_(@_)');
	if($ut_bruta_calc < 0)
		$sheet->getStyle("D$rownumber_tmp")->getFont()->getColor()->setARGB('FF0000');
    $row_number++;
    
	
	$gastos = 0;
	$cta_gastos = $ctas_functions->fetch_gastos_acounts();
	$sql = "SELECT cc.rowid, cc.cta, cc.descta, 0 as saldo, 'closed' as state FROM ".MAIN_DB_PREFIX."contab_cat_ctas as cc
			inner join ".MAIN_DB_PREFIX."contab_sat_ctas sc on cc.fk_sat_cta = sc.rowid
			where cc.rowid in (" . implode (", ", $cta_gastos) . ")";
	
	$rownumber_tmp = $row_number;
    $row_number++;
    
	$resql = $db->query($sql);
	$data_tmp = array();
	
	while($row = $db->fetch_object($resql)) 
	{
		$value = $ctas_functions->fetch_mounts_of_period($row->rowid, $startdate, $enddate);
		if($value["status"] == 1)
		{
			$row->saldo = strval($value["saldo"]);
			$gastos = $gastos + floatval($value["saldo"]);
			
			$sheet->setCellValue("A$row_number", $row->cta);
			$sheet->setCellValue("C$row_number", $row->descta);
			$sheet->setCellValue("D$row_number", floatval($value["saldo"]));
			$sheet->getStyle("D$row_number")->getNumberFormat()->setFormatCode('"$"* #,##0.00_-;_("$"* \(#,##0.00\);_("$"* "0.00"_);_(@_)');
			if(floatval($value["saldo"]) < 0)
				$sheet->getStyle("D$row_number")->getFont()->getColor()->setARGB('FF0000');
			
			$sheet->getStyle("A$row_number")->getAlignment()->setHorizontal('center');
			$sheet->getStyle("D$row_number")->getAlignment()->setHorizontal('center');
			$row_number++;
		}
		
	}
	
	$sheet->setCellValue("A$rownumber_tmp", "");
	$sheet->setCellValue("B$rownumber_tmp", "GASTOS DE OPERACIÓN");
	$sheet->setCellValue("D$rownumber_tmp", $gastos);
	$sheet->getStyle("A$rownumber_tmp")->getAlignment()->setHorizontal('center');
	$sheet->getStyle("D$rownumber_tmp")->getAlignment()->setHorizontal('center');
	$sheet->getStyle("A$rownumber_tmp:H$rownumber_tmp")->getFont()->setBold(true);
	$sheet->getStyle("D$rownumber_tmp")->getNumberFormat()->setFormatCode('"$"* #,##0.00_-;_("$"* \(#,##0.00\);_("$"* "0.00"_);_(@_)');
	if($gastos < 0)
		$sheet->getStyle("D$rownumber_tmp")->getFont()->getColor()->setARGB('FF0000');
	
	$row_number++;
	
	$costos = $costos + $gastos;
	
	
	$ut_operacion_calc = $ventas - $costos;
	$text_ut_operacion_calc = "UTILIDAD DE OPERACIÓN";
	if($ut_operacion_calc < 0)
		$text_ut_operacion_calc = "PERDIDA DE OPERACIÓN";
			
	$rownumber_tmp = $row_number;
    $row_number++;
    $sheet->setCellValue("A$rownumber_tmp", "");
	$sheet->setCellValue("B$rownumber_tmp", $text_ut_operacion_calc);
	$sheet->setCellValue("D$rownumber_tmp", $ut_operacion_calc);
	$sheet->getStyle("A$rownumber_tmp")->getAlignment()->setHorizontal('center');
	$sheet->getStyle("D$rownumber_tmp")->getAlignment()->setHorizontal('center');
	$sheet->getStyle("A$rownumber_tmp:H$rownumber_tmp")->getFont()->setBold(true);
	$sheet->getStyle("D$rownumber_tmp")->getNumberFormat()->setFormatCode('"$"* #,##0.00_-;_("$"* \(#,##0.00\);_("$"* "0.00"_);_(@_)');
	if($ut_operacion_calc < 0)
		$sheet->getStyle("D$rownumber_tmp")->getFont()->getColor()->setARGB('FF0000');
    $row_number++;
	
	
	$gastos = 0;
	$cta_gastos = $ctas_functions->fetch_gastos_acount();
	$sql = "SELECT cc.rowid, cc.cta, cc.descta, 0 as saldo, 'closed' as state FROM ".MAIN_DB_PREFIX."contab_cat_ctas as cc
			inner join ".MAIN_DB_PREFIX."contab_sat_ctas sc on cc.fk_sat_cta = sc.rowid
			where cc.subctade = $cta_gastos";
	
	$rownumber_tmp = $row_number;
    $row_number++;
    
	$resql = $db->query($sql);
	$data_tmp = array();
	
	while($row = $db->fetch_object($resql)) 
	{
		$value = $ctas_functions->fetch_mounts_of_period($row->rowid, $startdate, $enddate);
		if($value["status"] == 1)
		{
			$row->saldo = strval($value["saldo"]);
			$gastos = $gastos + floatval($value["saldo"]);
			
			$sheet->setCellValue("A$row_number", $row->cta);
			$sheet->setCellValue("C$row_number", $row->descta);
			$sheet->setCellValue("D$row_number", floatval($value["saldo"]));
			$sheet->getStyle("D$row_number")->getNumberFormat()->setFormatCode('"$"* #,##0.00_-;_("$"* \(#,##0.00\);_("$"* "0.00"_);_(@_)');
			if(floatval($value["saldo"]) < 0)
				$sheet->getStyle("D$row_number")->getFont()->getColor()->setARGB('FF0000');
			
			$sheet->getStyle("A$row_number")->getAlignment()->setHorizontal('center');
			$sheet->getStyle("D$row_number")->getAlignment()->setHorizontal('center');
			$row_number++;
		}
		
	}
	
	$sheet->setCellValue("A$rownumber_tmp", "");
	$sheet->setCellValue("B$rownumber_tmp", "RESULTADO INTEGRAL DE FINANCIAMIENTO");
	$sheet->setCellValue("D$rownumber_tmp", $gastos);
	$sheet->getStyle("A$rownumber_tmp")->getAlignment()->setHorizontal('center');
	$sheet->getStyle("D$rownumber_tmp")->getAlignment()->setHorizontal('center');
	$sheet->getStyle("A$rownumber_tmp:H$rownumber_tmp")->getFont()->setBold(true);
	$sheet->getStyle("D$rownumber_tmp")->getNumberFormat()->setFormatCode('"$"* #,##0.00_-;_("$"* \(#,##0.00\);_("$"* "0.00"_);_(@_)');
	if($gastos < 0)
		$sheet->getStyle("D$rownumber_tmp")->getFont()->getColor()->setARGB('FF0000');
	
	
	//SE LE PONE VALOR ABOSULUTO DONDE POSIBLEMENTE NO LLEVA
	$row_number++;
	$rownumber_tmp = $row_number;
	if($gastos < 0)
		$gastos = abs($gastos);
		
	$utilidad_antes_impuestos = $ut_operacion_calc - $gastos;
	$text_utilidad_antes_impuestos = "UTILIDAD ANTES DE IMPUESTOS A LA UTILIDAD";
	if($utilidad_antes_impuestos < 0)
		$text_utilidad_antes_impuestos = "PERDIDA ANTES DE IMPUESTOS A LA UTILIDAD";
	
	$sheet->setCellValue("A$rownumber_tmp", "");
	$sheet->setCellValue("B$rownumber_tmp", $text_utilidad_antes_impuestos);
	$sheet->setCellValue("D$rownumber_tmp", $utilidad_antes_impuestos);
	$sheet->getStyle("A$rownumber_tmp")->getAlignment()->setHorizontal('center');
	$sheet->getStyle("D$rownumber_tmp")->getAlignment()->setHorizontal('center');
	$sheet->getStyle("A$rownumber_tmp:H$rownumber_tmp")->getFont()->setBold(true);
	$sheet->getStyle("D$rownumber_tmp")->getNumberFormat()->setFormatCode('"$"* #,##0.00_-;_("$"* \(#,##0.00\);_("$"* "0.00"_);_(@_)');
	if($utilidad_antes_impuestos < 0)
		$sheet->getStyle("D$rownumber_tmp")->getFont()->getColor()->setARGB('FF0000');
	
	$row_number++;
	
	//Get all applicable Taxes
	$taxes = $ctas_functions->get_applicable_taxes();
	$utilidad_neta = $utilidad_antes_impuestos;
	for($i = 0; $i < count($taxes); $i++ )
	{
		$row_number++;
		$rownumber_tmp = $row_number;
		$utilidad_menos_impuesto = $utilidad_antes_impuestos * $taxes[$i]["value"];
		
		$sheet->setCellValue("A$rownumber_tmp", "");
		$sheet->setCellValue("B$rownumber_tmp", $taxes[$i]["name"]);
		$sheet->setCellValue("D$rownumber_tmp", $utilidad_menos_impuesto);
		$sheet->getStyle("A$rownumber_tmp")->getAlignment()->setHorizontal('center');
		$sheet->getStyle("D$rownumber_tmp")->getAlignment()->setHorizontal('center');
		$sheet->getStyle("A$rownumber_tmp:H$rownumber_tmp")->getFont()->setBold(true);
		$sheet->getStyle("D$rownumber_tmp")->getNumberFormat()->setFormatCode('"$"* #,##0.00_-;_("$"* \(#,##0.00\);_("$"* "0.00"_);_(@_)');
		if($utilidad_menos_impuesto < 0)
			$sheet->getStyle("D$rownumber_tmp")->getFont()->getColor()->setARGB('FF0000');
		$utilidad_neta = $utilidad_neta - $utilidad_menos_impuesto;
	}
	$row_number++;
	
	
	$row_number++;
	$rownumber_tmp = $row_number;
	$text_utilidad_neta = "UTILIDAD NETA";
	if($utilidad_neta < 0)
		$text_utilidad_neta = "PERDIDA NETA";
	
	$sheet->setCellValue("A$rownumber_tmp", "");
	$sheet->setCellValue("B$rownumber_tmp", $text_utilidad_neta);
	$sheet->setCellValue("D$rownumber_tmp", $utilidad_neta);
	$sheet->getStyle("A$rownumber_tmp")->getAlignment()->setHorizontal('center');
	$sheet->getStyle("D$rownumber_tmp")->getAlignment()->setHorizontal('center');
	$sheet->getStyle("A$rownumber_tmp:H$rownumber_tmp")->getFont()->setBold(true);
	$sheet->getStyle("D$rownumber_tmp")->getNumberFormat()->setFormatCode('"$"* #,##0.00_-;_("$"* \(#,##0.00\);_("$"* "0.00"_);_(@_)');
	if($utilidad_neta < 0)
		$sheet->getStyle("D$rownumber_tmp")->getFont()->getColor()->setARGB('FF0000');
		
	
	$sheet->getColumnDimension('A')->setAutoSize(true);
	$sheet->getColumnDimension('C')->setWidth(60);
    //$sheet->getColumnDimension('C')->setAutoSize(true);
    $sheet->getColumnDimension('C')->setAutoSize(true);
    $sheet->getColumnDimension('D')->setAutoSize(true);
    //$sheet->getColumnDimension('D')->setWidth(50);
    //$sheet->getColumnDimension('E')->setAutoSize(true);
    //$sheet->getColumnDimension('F')->setAutoSize(true);
    //$sheet->getColumnDimension('G')->setAutoSize(true);
    //$sheet->getColumnDimension('H')->setAutoSize(true);
	
	$writer = new Xlsx($spreadsheet);


header('Content-Type: application/vnd.ms-excel');
header('Content-Disposition: attachment;filename=EstadoResultados.xlsx'); /*-- $filename is  xsl filename ---*/
header('Cache-Control: max-age=0');
$writer->save('php://output');
?>