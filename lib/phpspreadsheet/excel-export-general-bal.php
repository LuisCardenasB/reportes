<?php


// Load Dolibarr environment
$res=0;
// Try main.inc.php into web root known defined into CONTEXT_DOCUMENT_ROOT (not always defined)
if (! $res && ! empty($_SERVER["CONTEXT_DOCUMENT_ROOT"])) $res=@include($_SERVER["CONTEXT_DOCUMENT_ROOT"]."/main.inc.php");
// Try main.inc.php into web root detected using web root caluclated from SCRIPT_FILENAME
$tmp=empty($_SERVER['SCRIPT_FILENAME'])?'':$_SERVER['SCRIPT_FILENAME'];$tmp2=realpath(__FILE__); $i=strlen($tmp)-1; $j=strlen($tmp2)-1;
while($i > 0 && $j > 0 && isset($tmp[$i]) && isset($tmp2[$j]) && $tmp[$i]==$tmp2[$j]) { $i--; $j--; }
if (! $res && $i > 0 && file_exists(substr($tmp, 0, ($i+1))."/main.inc.php")) $res=@include(substr($tmp, 0, ($i+1))."/main.inc.php");
if (! $res && $i > 0 && file_exists(dirname(substr($tmp, 0, ($i+1)))."/main.inc.php")) $res=@include(dirname(substr($tmp, 0, ($i+1)))."/main.inc.php");
// Try main.inc.php using relative path
if (! $res && file_exists("../../main.inc.php")) $res=@include("../../main.inc.php");
if (! $res && file_exists("../../../main.inc.php")) $res=@include("../../../main.inc.php");
if (! $res) die("Include of main fails");

//Custom Class
if (file_exists(DOL_DOCUMENT_ROOT . '/reports/class/accounts.class.php')) {
	require_once DOL_DOCUMENT_ROOT . '/reports/class/accounts.class.php';
}

if (file_exists(DOL_DOCUMENT_ROOT . '/reports/class/Utils.class.php')) {
	require_once DOL_DOCUMENT_ROOT . '/reports/class/Utils.class.php';
}

require 'vendor/autoload.php';

ini_set('memory_limit', '-1');

use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;

global $langs, $user, $conf, $db;

	$ctas_functions = new Accounts($db);
	$utils = new Utils($db);

	$startdate = GETPOST('startdate');
	$enddate = GETPOST('enddate');

	
    $spreadsheet = new Spreadsheet();
	$sheet = $spreadsheet->getActiveSheet();
	$sheet->setCellValue('A1', 'REPORTE DE BALANCE GENERAL');
	$sheet->mergeCells('A1:K1');
	$sheet->getStyle('A1:K1')->getAlignment()->setHorizontal('center');
	$sheet->getStyle('A1:K1')->getFont()->setBold(true);
	
	$sheet->setCellValue('A2', "PERIODO DEL $startdate AL $enddate");
	$sheet->mergeCells('A2:K2');
	$sheet->getStyle('A2:K2')->getAlignment()->setHorizontal('center');
	$sheet->getStyle('A2:K2')->getFont()->setBold(true);
	
	$sheet->setCellValue('A3', "MONEDA: PESO MEXICANO");
	$sheet->mergeCells('A3:K3');
	$sheet->getStyle('A3:K3')->getAlignment()->setHorizontal('center');
	$sheet->getStyle('A3:K3')->getFont()->setBold(true);
    $sheet->mergeCells('A4:K4');
    //Headers
    $sheet->setCellValue('A5', "Cuenta");
    $sheet->setCellValue('B5', "Nombre");
    $sheet->mergeCells('B5:D5');
    $sheet->setCellValue('E5', "Saldos");
    $sheet->getStyle('A5:E5')->getFont()->setBold(true);
    $sheet->getStyle('A5')->getAlignment()->setHorizontal('center');
    $sheet->getStyle('E5')->getAlignment()->setVertical('center');
    
    $sheet->setCellValue('G5', "Cuenta");
    $sheet->setCellValue('H5', "Nombre");
    $sheet->mergeCells('H5:J5');
    $sheet->setCellValue('K5', "Saldos");
    $sheet->getStyle('G5:K5')->getFont()->setBold(true);
    $sheet->getStyle('G5')->getAlignment()->setHorizontal('center');
    $sheet->getStyle('J5')->getAlignment()->setVertical('center');
    
    
    $row_number = 6;
    
    $activo = 0;
    $ctas = $ctas_functions->fetch_activos_acount();
    $sql = "select ctas.rowid as id, ctas.cta, ctas.descta, ctas.subctade as parentId, sctas.natur, pv, 0 as saldo
				from( select rowid,cta,descta,subctade,fk_sat_cta, @pv as pv 
					from ( 
				        select * from ".MAIN_DB_PREFIX."contab_cat_ctas 
				        order by subctade, rowid 
				    ) products_sorted, 
				   	(select @pv := $ctas) initialisation 
				where (find_in_set(subctade, @pv) > 0 or rowid = $ctas) 
				     and @pv := (case when ROUND((LENGTH(@pv) - LENGTH( REPLACE (@pv,\",\",\"\") ) ) / LENGTH(\",\")) < 3 then concat(@pv, ',', rowid) else @pv end)
				) ctas
				inner join ".MAIN_DB_PREFIX."contab_sat_ctas as sctas on ctas.fk_sat_cta = sctas.rowid
				order by ctas.rowid, ctas.subctade";

	$resql = $db->query($sql);
	$data_tmp = array();
	$l2 = 0;
	while($row = $db->fetch_object($resql)) 
	{
		$value = $ctas_functions->fetch_mounts_of_period($row->id, $startdate, $enddate);
		if($value["status"] == 1)
		{
			$row->saldo = strval($value["saldo"]);
			$activo = $activo + floatval($value["saldo"]);
			
			if($ctas == $row->id || floatval($value["saldo"]) != 0)
			{
				$sheet->setCellValue("A$row_number", $row->cta);
				if(substr_count($row->pv, ',') == 1)
				{
					$sheet->setCellValue("B$row_number", $row->descta);
					$sheet->mergeCells("B$row_number:D$row_number");
					$sheet->getStyle("B$row_number:E$row_number")->getFont()->setBold(true);
				}else if(substr_count($row->pv, ',') == 2 || $l2 == $row->parentId)
				{
					$l2 = $row->parentId;
					$sheet->setCellValue("C$row_number", $row->descta);
					$sheet->mergeCells("C$row_number:D$row_number");
					$sheet->getStyle("C$row_number:E$row_number")->getFont()->setBold(true);
				}else
				{
					$sheet->setCellValue("D$row_number", $row->descta);
				}
				$sheet->setCellValue("E$row_number", floatval($value["saldo"]));
				$sheet->getStyle("E$row_number")->getNumberFormat()->setFormatCode('"$"* #,##0.00_-;_("$"* \(#,##0.00\);_("$"* "0.00"_);_(@_)');
				if(floatval($value["saldo"]) < 0)
					$sheet->getStyle("E$row_number")->getFont()->getColor()->setARGB('FF0000');
				$sheet->getStyle("A$row_number")->getAlignment()->setHorizontal('center');
				$sheet->getStyle("E$row_number")->getAlignment()->setHorizontal('center');
				$row_number++;
			}
		}

	}
    
    $row_number++;
    //TOTAL ACTIVOS
    
    
    $row_number_c2 = 6;
    $pasivo = 0;
    $ctas = $ctas_functions->fetch_pasivo_acount();
    $sql = "select ctas.rowid as id, ctas.cta, ctas.descta, ctas.subctade as parentId, sctas.natur, pv, 0 as saldo
				from( select rowid,cta,descta,subctade,fk_sat_cta, @pv as pv 
					from ( 
				        select * from ".MAIN_DB_PREFIX."contab_cat_ctas 
				        order by subctade, rowid 
				    ) products_sorted, 
				   	(select @pv := $ctas) initialisation 
				where (find_in_set(subctade, @pv) > 0 or rowid = $ctas) 
				     and @pv := (case when ROUND((LENGTH(@pv) - LENGTH( REPLACE (@pv,\",\",\"\") ) ) / LENGTH(\",\")) < 3 then concat(@pv, ',', rowid) else @pv end)
				) ctas
				inner join ".MAIN_DB_PREFIX."contab_sat_ctas as sctas on ctas.fk_sat_cta = sctas.rowid";

	$resql = $db->query($sql);
	$data_tmp = array();
	$l2 = 0;
    while($row = $db->fetch_object($resql)) 
	{
		$value = $ctas_functions->fetch_mounts_of_period($row->id, $startdate, $enddate);
		if($value["status"] == 1)
		{
			$row->saldo = strval($value["saldo"]);
			$pasivo = $pasivo + floatval($value["saldo"]);
			
			if($ctas == $row->id || floatval($value["saldo"]) != 0)
			{
				$sheet->setCellValue("G$row_number_c2", $row->cta);
				if(substr_count($row->pv, ',') == 1)
				{
					$sheet->setCellValue("H$row_number_c2", $row->descta);
					$sheet->mergeCells("H$row_number_c2:J$row_number_c2");
					$sheet->getStyle("H$row_number_c2:K$row_number_c2")->getFont()->setBold(true);
				}else if(substr_count($row->pv, ',') == 2 || $l2 == $row->parentId)
				{
					$l2 = $row->parentId;
					$sheet->setCellValue("I$row_number_c2", $row->descta);
					$sheet->mergeCells("I$row_number_c2:J$row_number_c2");
					$sheet->getStyle("H$row_number_c2:K$row_number_c2")->getFont()->setBold(true);
				}else
				{
					$sheet->setCellValue("J$row_number_c2", $row->descta);
				}
				$sheet->setCellValue("K$row_number_c2", floatval($value["saldo"]));
				$sheet->getStyle("K$row_number_c2")->getNumberFormat()->setFormatCode('"$"* #,##0.00_-;_("$"* \(#,##0.00\);_("$"* "0.00"_);_(@_)');
				if(floatval($value["saldo"]) < 0)
					$sheet->getStyle("K$row_number_c2")->getFont()->getColor()->setARGB('FF0000');
				$sheet->getStyle("G$row_number_c2")->getAlignment()->setHorizontal('center');
				$sheet->getStyle("K$row_number_c2")->getAlignment()->setHorizontal('center');
				$row_number_c2++;
			}
		}

	}
    
    $row_number_c2++;
    $capital = 0;
    $ctas = $ctas_functions->fetch_capital_acount();
    $sql = "select ctas.rowid as id, ctas.cta, ctas.descta, ctas.subctade as parentId, sctas.natur, pv, 0 as saldo
				from( select rowid,cta,descta,subctade,fk_sat_cta, @pv as pv 
					from ( 
				        select * from ".MAIN_DB_PREFIX."contab_cat_ctas 
				        order by subctade, rowid 
				    ) products_sorted, 
				   	(select @pv := $ctas) initialisation 
				where (find_in_set(subctade, @pv) > 0 or rowid = $ctas) 
				     and @pv := (case when ROUND((LENGTH(@pv) - LENGTH( REPLACE (@pv,\",\",\"\") ) ) / LENGTH(\",\")) < 3 then concat(@pv, ',', rowid) else @pv end)
				) ctas
				inner join ".MAIN_DB_PREFIX."contab_sat_ctas as sctas on ctas.fk_sat_cta = sctas.rowid";
	
	$resql = $db->query($sql);
	$data_tmp = array();
	$l2 = 0; 
    while($row = $db->fetch_object($resql)) 
	{
		//$value = $ctas_functions->fetch_mounts_of_period($row->id, $startdate, $enddate);
		$value = $ctas_functions->fetch_mounts_of_period_from_some_accounts($row->id, $startdate, $enddate);
		$edo_res = $ctas_functions->get_edo_results($startdate, $enddate);
		if($ctas == $row->id)
		{
			
			$row->saldo = floatval($value) + floatval($edo_res);
			$capital = $capital + $row->saldo;
		}
		else 
		{
			//$row->saldo = strval($value["saldo"]);
			//$capital = $capital + floatval($value["saldo"]);
			
			if($row->id == $ctas_functions->fetch_edo_result_acount())
			{
				if($value != 0)
				{
					$row->saldo = $edo_res + $value;
					$capital = $capital + floatval($row->saldo);
				}else
				{
					$row->saldo = $edo_res;
					$capital = $capital + $row->saldo;
				}
			}else
			{
				$row->saldo = strval($value);
			}
		}
		
		if($ctas == $row->id || floatval($row->saldo) != 0)
		{
			$sheet->setCellValue("G$row_number_c2", $row->cta);
			if(substr_count($row->pv, ',') == 1)
			{
				$sheet->setCellValue("H$row_number_c2", $row->descta);
				$sheet->mergeCells("H$row_number_c2:J$row_number_c2");
				$sheet->getStyle("H$row_number_c2:K$row_number_c2")->getFont()->setBold(true);
			}else if(substr_count($row->pv, ',') == 2 || $l2 == $row->parentId)
			{
				$l2 = $row->parentId;
				$sheet->setCellValue("I$row_number_c2", $row->descta);
				$sheet->mergeCells("I$row_number_c2:J$row_number_c2");
				$sheet->getStyle("H$row_number_c2:K$row_number_c2")->getFont()->setBold(true);
			}else
			{
				$sheet->setCellValue("J$row_number_c2", $row->descta);
			}
			$sheet->setCellValue("K$row_number_c2", floatval($row->saldo));
			$sheet->getStyle("K$row_number_c2")->getNumberFormat()->setFormatCode('"$"* #,##0.00_-;_("$"* \(#,##0.00\);_("$"* "0.00"_);_(@_)');
			if(floatval($row->saldo) < 0)
				$sheet->getStyle("K$row_number_c2")->getFont()->getColor()->setARGB('FF0000');
			$sheet->getStyle("G$row_number_c2")->getAlignment()->setHorizontal('center');
			$sheet->getStyle("K$row_number_c2")->getAlignment()->setHorizontal('center');
			$row_number_c2++;
		}

	}
	
	$row_number_c2++;
    
    if($row_number_c2 > $row_number)
    	$row_number = $row_number_c2;
    
    $activo = $ctas_functions->get_activo_results($startdate, $enddate);
    $tot_cap_pas = $ctas_functions->get_pasivo_capital_results($startdate, $enddate);
    
    $sheet->setCellValue("B$row_number", "TOTAL ACTIVO");
	$sheet->mergeCells("B$row_number:D$row_number");
	$sheet->getStyle("A$row_number:E$row_number")->getFont()->setBold(true);
    $sheet->setCellValue("E$row_number", floatval($activo));
	$sheet->getStyle("E$row_number")->getNumberFormat()->setFormatCode('"$"* #,##0.00_-;_("$"* \(#,##0.00\);_("$"* "0.00"_);_(@_)');
	if(floatval($activo) < 0)
		$sheet->getStyle("E$row_number")->getFont()->getColor()->setARGB('FF0000');
	$sheet->getStyle("A$row_number")->getAlignment()->setHorizontal('center');
	$sheet->getStyle("E$row_number")->getAlignment()->setHorizontal('center');
	
	
	$sheet->setCellValue("H$row_number", "TOTAL PASIVO Y CAPITAL");
	$sheet->mergeCells("H$row_number:J$row_number");
	$sheet->getStyle("G$row_number:K$row_number")->getFont()->setBold(true);
    $sheet->setCellValue("K$row_number", floatval($tot_cap_pas));
	$sheet->getStyle("K$row_number")->getNumberFormat()->setFormatCode('"$"* #,##0.00_-;_("$"* \(#,##0.00\);_("$"* "0.00"_);_(@_)');
	if(floatval($tot_cap_pas) < 0)
		$sheet->getStyle("K$row_number")->getFont()->getColor()->setARGB('FF0000');
	$sheet->getStyle("G$row_number")->getAlignment()->setHorizontal('center');
	$sheet->getStyle("K$row_number")->getAlignment()->setHorizontal('center');
    
	
	$sheet->getColumnDimension('A')->setAutoSize(true);
	$sheet->getColumnDimension('B')->setWidth(3);
	$sheet->getColumnDimension('C')->setWidth(3);
    $sheet->getColumnDimension('D')->setAutoSize(true);
    $sheet->getColumnDimension('E')->setAutoSize(true);
    $sheet->getColumnDimension('F')->setWidth(3);
    $sheet->getColumnDimension('G')->setAutoSize(true);
	$sheet->getColumnDimension('H')->setWidth(3);
	$sheet->getColumnDimension('I')->setWidth(3);
    $sheet->getColumnDimension('J')->setAutoSize(true);
    $sheet->getColumnDimension('K')->setAutoSize(true);
	
	$writer = new Xlsx($spreadsheet);


header('Content-Type: application/vnd.ms-excel');
header('Content-Disposition: attachment;filename=BalanceGeneral.xlsx'); /*-- $filename is  xsl filename ---*/
header('Cache-Control: max-age=0');
$writer->save('php://output');
?>