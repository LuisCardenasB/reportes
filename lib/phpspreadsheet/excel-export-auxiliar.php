<?php


// Load Dolibarr environment
$res=0;
// Try main.inc.php into web root known defined into CONTEXT_DOCUMENT_ROOT (not always defined)
if (! $res && ! empty($_SERVER["CONTEXT_DOCUMENT_ROOT"])) $res=@include($_SERVER["CONTEXT_DOCUMENT_ROOT"]."/main.inc.php");
// Try main.inc.php into web root detected using web root caluclated from SCRIPT_FILENAME
$tmp=empty($_SERVER['SCRIPT_FILENAME'])?'':$_SERVER['SCRIPT_FILENAME'];$tmp2=realpath(__FILE__); $i=strlen($tmp)-1; $j=strlen($tmp2)-1;
while($i > 0 && $j > 0 && isset($tmp[$i]) && isset($tmp2[$j]) && $tmp[$i]==$tmp2[$j]) { $i--; $j--; }
if (! $res && $i > 0 && file_exists(substr($tmp, 0, ($i+1))."/main.inc.php")) $res=@include(substr($tmp, 0, ($i+1))."/main.inc.php");
if (! $res && $i > 0 && file_exists(dirname(substr($tmp, 0, ($i+1)))."/main.inc.php")) $res=@include(dirname(substr($tmp, 0, ($i+1)))."/main.inc.php");
// Try main.inc.php using relative path
if (! $res && file_exists("../../main.inc.php")) $res=@include("../../main.inc.php");
if (! $res && file_exists("../../../main.inc.php")) $res=@include("../../../main.inc.php");
if (! $res) die("Include of main fails");

//Custom Class
if (file_exists(DOL_DOCUMENT_ROOT . '/reports/class/accounts.class.php')) {
	require_once DOL_DOCUMENT_ROOT . '/reports/class/accounts.class.php';
}

require 'vendor/autoload.php';

use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;

global $langs, $user, $conf, $db;

	$ctas_functions = new Accounts($db);

	$cta = GETPOST('cta');
	$startdate = GETPOST('startdate');
	$enddate = GETPOST('enddate');
	$cta2 = GETPOST('cta2');

    $spreadsheet = new Spreadsheet();
	$sheet = $spreadsheet->getActiveSheet();
	$sheet->setCellValue('A1', 'REPORTE AUXILIAR DE CUENTAS');
	$sheet->mergeCells('A1:H1');
	$sheet->getStyle('A1:H1')->getAlignment()->setHorizontal('center');
	$sheet->getStyle('A1:H1')->getFont()->setBold(true);
	
	$sheet->setCellValue('A2', "MOVIMIENTOS DEL $startdate AL $enddate");
	$sheet->mergeCells('A2:H2');
	$sheet->getStyle('A2:H2')->getAlignment()->setHorizontal('center');
	$sheet->getStyle('A2:H2')->getFont()->setBold(true);
	
	$sheet->setCellValue('A3', "MONEDA: PESO MEXICANO");
	$sheet->mergeCells('A3:H3');
	$sheet->getStyle('A3:H3')->getAlignment()->setHorizontal('center');
	$sheet->getStyle('A3:H3')->getFont()->setBold(true);
    $sheet->mergeCells('A4:H4');
    //Headers
    $sheet->setCellValue('A5', "Cuenta");
    $sheet->mergeCells('A5:B5');
    $sheet->setCellValue('A6', "Fecha");
    $sheet->setCellValue('B6', "Tipo");
    
    $sheet->setCellValue('C5', "Nombre");
    $sheet->mergeCells('C5:D5');
    $sheet->setCellValue('C6', "Número");
    $sheet->setCellValue('D6', "Concepto");
    
    $sheet->setCellValue('E5', "Referencia");
    $sheet->mergeCells('E5:E6');
    $sheet->setCellValue('F5', "Cargos");
    $sheet->mergeCells('F5:F6');
    $sheet->setCellValue('G5', "Abonos");
    $sheet->mergeCells('G5:G6');
    $sheet->setCellValue('H5', "Saldo inicial");
    $sheet->setCellValue('H6', "Saldos");
    $sheet->getStyle('A5:H6')->getFont()->setBold(true);
    $sheet->getStyle('A5:H6')->getAlignment()->setHorizontal('center');
    $sheet->getStyle('A5:H6')->getAlignment()->setVertical('center');
    
    //Get parent
	$sql = "select subctade from ".MAIN_DB_PREFIX."contab_cat_ctas where rowid = '" . $cta . "' ";
	$resql = $db->query($sql);
	$parent = "";
	while($row = $db->fetch_object($resql)) 
	{
		$parent = " and subctade = " . $row->subctade;
	}
	//Get Accounts names
	$sql = "SELECT cta FROM ".MAIN_DB_PREFIX."contab_cat_ctas where rowid = '" . $cta . "'";
	$resql = $db->query($sql);
	$cta_desc = "";
	while($row = $db->fetch_object($resql)) 
	{
		$cta_desc = $row->cta;
	}
	$sql_accounts = "SELECT rowid FROM ".MAIN_DB_PREFIX."contab_cat_ctas where cta = '" . $cta_desc . "'";
	
	if($cta2 > 0)
	{
		$sql = "SELECT cta FROM ".MAIN_DB_PREFIX."contab_cat_ctas where rowid = '" . $cta2 . "'";
		$resql = $db->query($sql);
		$cta2_desc = "";
		while($row = $db->fetch_object($resql)) 
		{
			$cta2_desc = $row->cta;
		}
		$sql_accounts = "SELECT rowid FROM ".MAIN_DB_PREFIX."contab_cat_ctas where cta between '" . $cta_desc . "' and '" . $cta2_desc . "' $parent ";
	}
	
	//Get parents
	$resql = $db->query($sql_accounts);
	$accounts = array();
	while($row = $db->fetch_object($resql)) 
	{
		array_push($accounts, $row->rowid);
	}
    
    $row_number = 7;
    for($i = 0; $i < count($accounts); $i++)
    {
    	$sql = "select *, (debe-haber) as saldo from (
	    		select cuenta as rowid, cta, descta, cuenta, sum(debe) as debe, sum(haber) as haber, 'closed' as state from( 
	    		select rowid,cta,descta,subctade 
	    			from (
	    				select * from llx_contab_cat_ctas 
	    				order by subctade, rowid
	    			) products_sorted, 
	    			(select @pv := ".$accounts[$i].") initialisation 
					where (find_in_set(subctade, @pv) > 0 or rowid = ".$accounts[$i].") and @pv := concat(@pv, ',', rowid)) ctas 
	    			inner join llx_contab_polizasdet pdet on pdet.cuenta = ctas.cta 
					INNER JOIN llx_contab_polizas as p on pdet.fk_poliza = p.rowid
					where p.fecha between '".$startdate."' and '".$enddate."'
				group by cta, descta, cuenta ) result ";
				
	    $resql = $db->query($sql);
		while($row = $db->fetch_object($resql)) 
		{
			//GetSaldoInicial
			$saldo_inicial = $ctas_functions->fetch_initial_mounts($row->rowid, $startdate);
			if($saldo_inicial["status"] == 1)
				$sheet->setCellValue("H$row_number", $saldo_inicial["saldo"]);
			else
				$sheet->setCellValue("H$row_number", 0);
			
			$sheet->setCellValue("A$row_number", $row->cta);
			$sheet->mergeCells("A$row_number:B$row_number");
			$sheet->setCellValue("C$row_number", $row->descta);
			$sheet->mergeCells("C$row_number:D$row_number");
			$sheet->getStyle("A$row_number:B$row_number")->getAlignment()->setHorizontal('center');
			$sheet->getStyle("E$row_number:H$row_number")->getAlignment()->setHorizontal('center');
			$sheet->getStyle("A$row_number:H$row_number")->getFont()->setBold(true);
			
			//Detail
			$row_number++;
			$sql_sub = "select *, (debe - haber) as saldo from(
	        		select rowid, fecha, tipo_pol, cons, descta, sum(debe) as debe, sum(haber) as haber from (
						SELECT p.rowid as rowid, p.fecha, p.tipo_pol, p.cons, p.concepto as descta, det.debe, det.haber FROM llx_contab_polizasdet as det
						INNER JOIN llx_contab_polizas as p on det.fk_poliza = p.rowid
						INNER JOIN llx_contab_cat_ctas as cta on det.cuenta = cta.cta
						where p.fecha between '".$startdate."' and '".$enddate."' and cta = '".$row->rowid."'
					order by fecha ) result group by rowid, fecha, tipo_pol, cons, descta ) result order by fecha";
			$resql_sub = $db->query($sql_sub);
			while($row_sub = $db->fetch_object($resql_sub)) 
			{
				$sheet->setCellValue("A$row_number", $row_sub->fecha);
				$sheet->setCellValue("B$row_number", $row_sub->tipo_pol);
				$sheet->setCellValue("C$row_number", $row_sub->cons);
				$sheet->setCellValue("D$row_number", $row_sub->descta);
				$sheet->setCellValue("E$row_number", "");
				$sheet->setCellValue("F$row_number", $row_sub->debe);
				$sheet->setCellValue("G$row_number", $row_sub->haber);
				$sheet->setCellValue("H$row_number", $row_sub->saldo);
				$sheet->getStyle("F$row_number")->getNumberFormat()->setFormatCode('$ #,##0.00_-');
				$sheet->getStyle("G$row_number")->getNumberFormat()->setFormatCode('$ #,##0.00_-');
				$sheet->getStyle("H$row_number")->getNumberFormat()->setFormatCode('$ #,##0.00_-');
				if($row_sub->saldo < 0)
					$sheet->getStyle("H$row_number")->getFont()->getColor()->setARGB('FF0000');
				
				$sheet->getStyle("A$row_number:B$row_number")->getAlignment()->setHorizontal('center');
				$sheet->getStyle("E$row_number:H$row_number")->getAlignment()->setHorizontal('center');
				$row_number++;
			}
			
			$total = floatval($row->saldo) + floatval($saldo_inicial["saldo"]);
			//Adding totals
			$sheet->setCellValue("E$row_number", "TOTAL:");
			$sheet->setCellValue("F$row_number", $row->debe);
			$sheet->setCellValue("G$row_number", $row->haber);
			$sheet->setCellValue("H$row_number", $total);
			$sheet->getStyle("F$row_number")->getNumberFormat()->setFormatCode('$ #,##0.00_-');
			$sheet->getStyle("G$row_number")->getNumberFormat()->setFormatCode('$ #,##0.00_-');
			$sheet->getStyle("H$row_number")->getNumberFormat()->setFormatCode('$ #,##0.00_-');
			if($total < 0)
				$sheet->getStyle("H$row_number")->getFont()->getColor()->setARGB('FF0000');
					
			$sheet->getStyle("A$row_number:B$row_number")->getAlignment()->setHorizontal('center');
			$sheet->getStyle("E$row_number:H$row_number")->getAlignment()->setHorizontal('center');
			$sheet->getStyle("A$row_number:H$row_number")->getFont()->setBold(true);
			$row_number++;
		}	
    }
	
	$sheet->getColumnDimension('A')->setAutoSize(true);
    $sheet->getColumnDimension('B')->setAutoSize(true);
    $sheet->getColumnDimension('C')->setAutoSize(true);
    $sheet->getColumnDimension('D')->setWidth(50);
    $sheet->getColumnDimension('E')->setAutoSize(true);
    $sheet->getColumnDimension('F')->setAutoSize(true);
    $sheet->getColumnDimension('G')->setAutoSize(true);
    $sheet->getColumnDimension('H')->setAutoSize(true);
	
	$writer = new Xlsx($spreadsheet);


header('Content-Type: application/vnd.ms-excel');
header('Content-Disposition: attachment;filename=Auxiliar.xlsx'); /*-- $filename is  xsl filename ---*/
header('Cache-Control: max-age=0');
$writer->save('php://output');
?>