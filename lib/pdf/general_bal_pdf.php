<?php

	// Load Dolibarr environment
	$res=0;
	// Try main.inc.php into web root known defined into CONTEXT_DOCUMENT_ROOT (not always defined)
	if (! $res && ! empty($_SERVER["CONTEXT_DOCUMENT_ROOT"])) $res=@include($_SERVER["CONTEXT_DOCUMENT_ROOT"]."/main.inc.php");
	// Try main.inc.php into web root detected using web root caluclated from SCRIPT_FILENAME
	$tmp=empty($_SERVER['SCRIPT_FILENAME'])?'':$_SERVER['SCRIPT_FILENAME'];$tmp2=realpath(__FILE__); $i=strlen($tmp)-1; $j=strlen($tmp2)-1;
	while($i > 0 && $j > 0 && isset($tmp[$i]) && isset($tmp2[$j]) && $tmp[$i]==$tmp2[$j]) { $i--; $j--; }
	if (! $res && $i > 0 && file_exists(substr($tmp, 0, ($i+1))."/main.inc.php")) $res=@include(substr($tmp, 0, ($i+1))."/main.inc.php");
	if (! $res && $i > 0 && file_exists(dirname(substr($tmp, 0, ($i+1)))."/main.inc.php")) $res=@include(dirname(substr($tmp, 0, ($i+1)))."/main.inc.php");
	// Try main.inc.php using relative path
	if (! $res && file_exists("../../main.inc.php")) $res=@include("../../main.inc.php");
	if (! $res && file_exists("../../../main.inc.php")) $res=@include("../../../main.inc.php");
	if (! $res) die("Include of main fails");
	
	include __DIR__ . '/../vendor/autoload.php';
	
	if (file_exists(DOL_DOCUMENT_ROOT . '/reports/class/accounts.class.php')) {
		require_once DOL_DOCUMENT_ROOT . '/reports/class/accounts.class.php';
	}

	global $langs, $user, $conf, $db;
	
	$ctas_functions = new Accounts($db);
	
	$startdate = GETPOST('startdate');
    $enddate = GETPOST('enddate');
    
    $activo_table = "";
    $activo = 0;
    $ctas = $ctas_functions->fetch_activos_acount();
    $sql = "select ctas.rowid as id, ctas.cta, ctas.descta, ctas.subctade as parentId, sctas.natur, pv, 0 as saldo
				from( select rowid,cta,descta,subctade,fk_sat_cta, @pv as pv 
					from ( 
				        select * from ".MAIN_DB_PREFIX."contab_cat_ctas 
				        order by subctade, rowid 
				    ) products_sorted, 
				   	(select @pv := $ctas) initialisation 
				where (find_in_set(subctade, @pv) > 0 or rowid = $ctas) 
				     and @pv := (case when ROUND((LENGTH(@pv) - LENGTH( REPLACE (@pv,\",\",\"\") ) ) / LENGTH(\",\")) < 3 then concat(@pv, ',', rowid) else @pv end)
				) ctas
				inner join ".MAIN_DB_PREFIX."contab_sat_ctas as sctas on ctas.fk_sat_cta = sctas.rowid
				order by ctas.rowid, ctas.subctade";

	$resql = $db->query($sql);
	$data_tmp = array();
	$l2 = 0;
	while($row = $db->fetch_object($resql)) 
	{
		$value = $ctas_functions->fetch_mounts_of_period($row->id, $startdate, $enddate);
		if($value["status"] == 1)
		{
			$row->saldo = strval($value["saldo"]);
			$activo = $activo + floatval($value["saldo"]);
			
			if($ctas == $row->id || floatval($value["saldo"]) != 0)
			{
				if(substr_count($row->pv, ',') == 1)
				{
					$activo_table .= 
						"
							<tr class='cabecera'>
								<td class='center'>".$row->cta."</td>
								<td colspan='3' class='left'>".$row->descta."</td>
								<td class='center' ".$ctas_functions->getClassColor($value["saldo"]).">".$ctas_functions->getNumberFormat($value["saldo"])."</td>
							</tr>
						";
					
				}else if(substr_count($row->pv, ',') == 2 || $l2 == $row->parentId)
				{
					$l2 = $row->parentId;
					$activo_table .= 
						"
							<tr class='cuentas'>
								<td class='center'>".$row->cta."</td>
								<td class='center'></td>
								<td colspan='2' class='left'>".$row->descta."</td>
								<td class='center' ".$ctas_functions->getClassColor($value["saldo"]).">".$ctas_functions->getNumberFormat($value["saldo"])."</td>
							</tr>
						";
				}else
				{
					$activo_table .= 
						"
							<tr class='cuentas'>
								<td class='center'>".$row->cta."</td>
								<td class='center'></td>
								<td class='center'></td>
								<td class='left'>".$row->descta."</td>
								<td class='center' ".$ctas_functions->getClassColor($value["saldo"]).">".$ctas_functions->getNumberFormat($value["saldo"])."</td>
							</tr>
						";
				}
			}
		}

	}
	
	$pasivo_table = "";
	$pasivo = 0;
    $ctas = $ctas_functions->fetch_pasivo_acount();
    $sql = "select ctas.rowid as id, ctas.cta, ctas.descta, ctas.subctade as parentId, sctas.natur, pv, 0 as saldo
				from( select rowid,cta,descta,subctade,fk_sat_cta, @pv as pv 
					from ( 
				        select * from ".MAIN_DB_PREFIX."contab_cat_ctas 
				        order by subctade, rowid 
				    ) products_sorted, 
				   	(select @pv := $ctas) initialisation 
				where (find_in_set(subctade, @pv) > 0 or rowid = $ctas) 
				     and @pv := (case when ROUND((LENGTH(@pv) - LENGTH( REPLACE (@pv,\",\",\"\") ) ) / LENGTH(\",\")) < 3 then concat(@pv, ',', rowid) else @pv end)
				) ctas
				inner join ".MAIN_DB_PREFIX."contab_sat_ctas as sctas on ctas.fk_sat_cta = sctas.rowid";

	$resql = $db->query($sql);
	$data_tmp = array();
	$l2 = 0;
    while($row = $db->fetch_object($resql)) 
	{
		$value = $ctas_functions->fetch_mounts_of_period($row->id, $startdate, $enddate);
		if($value["status"] == 1)
		{
			$row->saldo = strval($value["saldo"]);
			$pasivo = $pasivo + floatval($value["saldo"]);
			
			if($ctas == $row->id || floatval($value["saldo"]) != 0)
			{
				if(substr_count($row->pv, ',') == 1)
				{
					$pasivo_table .= 
						"
							<tr class='cabecera'>
								<td class='center'>".$row->cta."</td>
								<td colspan='3' class='left'>".$row->descta."</td>
								<td class='center' ".$ctas_functions->getClassColor($value["saldo"]).">".$ctas_functions->getNumberFormat($value["saldo"])."</td>
							</tr>
						";
					
				}else if(substr_count($row->pv, ',') == 2 || $l2 == $row->parentId)
				{
					$l2 = $row->parentId;
					$pasivo_table .= 
						"
							<tr class='cuentas'>
								<td class='center'>".$row->cta."</td>
								<td class='center'></td>
								<td colspan='2' class='left'>".$row->descta."</td>
								<td class='center' ".$ctas_functions->getClassColor($value["saldo"]).">".$ctas_functions->getNumberFormat($value["saldo"])."</td>
							</tr>
						";
				}else
				{
					$pasivo_table .= 
						"
							<tr class='cuentas'>
								<td class='center'>".$row->cta."</td>
								<td class='center'></td>
								<td class='center'></td>
								<td class='left'>".$row->descta."</td>
								<td class='center' ".$ctas_functions->getClassColor($value["saldo"]).">".$ctas_functions->getNumberFormat($value["saldo"])."</td>
							</tr>
						";
				}
				
			}
		}

	}
    
    $capital_table = "";
    $capital = 0;
    $ctas = $ctas_functions->fetch_capital_acount();
    $sql = "select ctas.rowid as id, ctas.cta, ctas.descta, ctas.subctade as parentId, sctas.natur, pv, 0 as saldo
				from( select rowid,cta,descta,subctade,fk_sat_cta, @pv as pv 
					from ( 
				        select * from ".MAIN_DB_PREFIX."contab_cat_ctas 
				        order by subctade, rowid 
				    ) products_sorted, 
				   	(select @pv := $ctas) initialisation 
				where (find_in_set(subctade, @pv) > 0 or rowid = $ctas) 
				     and @pv := (case when ROUND((LENGTH(@pv) - LENGTH( REPLACE (@pv,\",\",\"\") ) ) / LENGTH(\",\")) < 3 then concat(@pv, ',', rowid) else @pv end)
				) ctas
				inner join ".MAIN_DB_PREFIX."contab_sat_ctas as sctas on ctas.fk_sat_cta = sctas.rowid";
	
	$resql = $db->query($sql);
	$data_tmp = array();
	$l2 = 0; 
    while($row = $db->fetch_object($resql)) 
	{
		//$value = $ctas_functions->fetch_mounts_of_period($row->id, $startdate, $enddate);
		$value = $ctas_functions->fetch_mounts_of_period_from_some_accounts($row->id, $startdate, $enddate);
		$edo_res = $ctas_functions->get_edo_results($startdate, $enddate);
		if($ctas == $row->id)
		{
			
			$row->saldo = floatval($value) + floatval($edo_res);
			$capital = $capital + $row->saldo;
		}
		else 
		{
			//$row->saldo = strval($value["saldo"]);
			//$capital = $capital + floatval($value["saldo"]);
			
			if($row->id == $ctas_functions->fetch_edo_result_acount())
			{
				if($value != 0)
				{
					$row->saldo = $edo_res + $value;
					$capital = $capital + floatval($row->saldo);
				}else
				{
					$row->saldo = $edo_res;
					$capital = $capital + $row->saldo;
				}
			}else
			{
				$row->saldo = strval($value);
			}
		}
		
		if($ctas == $row->id || floatval($row->saldo) != 0)
		{
			if(substr_count($row->pv, ',') == 1)
			{
				$capital_table .= 
					"
						<tr class='cabecera'>
							<td class='center'>".$row->cta."</td>
							<td colspan='3' class='left'>".$row->descta."</td>
							<td class='center' ".$ctas_functions->getClassColor($row->saldo).">".$ctas_functions->getNumberFormat($row->saldo)."</td>
						</tr>
					";
				
			}else if(substr_count($row->pv, ',') == 2 || $l2 == $row->parentId)
			{
				$l2 = $row->parentId;
				$capital_table .= 
					"
						<tr class='cuentas'>
							<td class='center'>".$row->cta."</td>
							<td class='center'></td>
							<td colspan='2' class='left'>".$row->descta."</td>
							<td class='center' ".$ctas_functions->getClassColor($row->saldo).">".$ctas_functions->getNumberFormat($row->saldo)."</td>
						</tr>
					";
			}else
			{
				$capital_table .= 
					"
						<tr class='cuentas'>
							<td class='center'>".$row->cta."</td>
							<td class='center'></td>
							<td class='center'></td>
							<td class='left'>".$row->descta."</td>
							<td class='center' ".$ctas_functions->getClassColor($row->saldo).">".$ctas_functions->getNumberFormat($row->saldo)."</td>
						</tr>
					";
			}
		}

	}
    
    
    $activo = $ctas_functions->get_activo_results($startdate, $enddate);
    $tot_cap_pas = $ctas_functions->get_pasivo_capital_results($startdate, $enddate);
    
    
    
	
	$logo = (isset($_SERVER['HTTPS']) ? "https://" : "http://") . $_SERVER['HTTP_HOST'] . DOL_URL_ROOT .'/documents/mycompany/logos/thumbs/'. $mysoc->logo_mini;
	
	$html_start .= 
		"<!DOCTYPE html>
		<html>
			<head>
				<style>
					html,body{
						font-family:DejaVuSans; 
						font-size:9px;
					}
					#company{
						font-size:10px
					}
					#company .title{
						font-size:12px
					} 
					#invoice
					{
						max-width:800px;
						margin:0 auto;
					}
					#company{
						width:100%;
						border-collapse:collapse;
					}
					#company td{
						padding:3px
					}
					#company img{
						max-width:180px;
						height:auto
					}
					.right{
						text-align:right
					}
					.center{
						text-align:center
					}
					#company td{
						width:25%;
					}
					#company td .tit{
						width:50%;
					}
					.content
					{
						font-size:10px;
						width: 100%;
						border-collapse:collapse;
					}
					.content tr td{
						font-size:10px;
						//text-align:center;
						padding: 6px;
					}
					.content td{
						border-bottom:1px solid #0879BF;
					}
					.content th{
						border-bottom:1px solid #0879BF;
						font-weight: bold;
						//text-align:center;
						padding: 2px;
					}
					.content .cabecera td{
						font-weight: bold;
						border: none;
						background-color: #f2f2f2;
					}
					.content .cuentas td{
						border: none;
						font-size:10px;
						padding: 1px;
						border-bottom: 0.3px solid #bfbfbf;
					}
					.content .totales td{
						background-color: #f2f2f2;
						font-weight: bold;
						border-bottom: 0.5px solid #bfbfbf;
					}
					.negative-money{
						color: red;
					}
					.bal-gen{
						width: 100%;
					}
					.tables-td-bal{
						width: 50%; 
						vertical-align: top;
					}
					.bottom-data{
						font-weight: bold;
						font-size:10px;
					}
					.bottom-data span{
						font-weight: bold;
						font-size:14px;
					}
					
				</style>
			</head>
		<body>
			<div id='invoice'>
				<table id='company'>
					<tr>
						<td rowspan='3'><img src='".$logo."'/></td>
						<td class='center title'><strong>REPORTE DE BALANCE GENERAL</strong></td>
						<td></td>
					</tr>
					<tr>
						<td class='center'>MOVIMIENTOS DEL <strong>$startdate</strong> AL <strong>$enddate</strong></td>
						<td></td>
					</tr>
					<tr>
						<td class='center'>MONEDA: PESO MEXICANO</td>
						<td></td>
					</tr>
				</table>
				
				<br>
		";
	$html_table_declaration = 
		"	
				<table class='bal-gen'>
					<tr>
						<td rowspan='2' class='tables-td-bal'>
							<table class='content'>
								<tr>
									<th class='center'>CUENTA</th>
									<th colspan='3' class='left'>NOMBRE</th>
									<th class='center'>SALDOS</th>
								</tr>
								
								".$activo_table."
								
							</table>
						</td>
						<td class='tables-td-bal'>
							<table class='content'>
								<tr>
									<th class='center'>CUENTA</th>
									<th colspan='3' class='left'>NOMBRE</th>
									<th class='center'>SALDOS</th>
								</tr>
								
								".$pasivo_table."
								
							</table>
						</td>
					</tr>
					<br>
					<br>
					<tr>
						<td class='tables-td-bal'>
							<table class='content'>
								<tr>
									<th class='center'>CUENTA</th>
									<th colspan='3' class='left'>NOMBRE</th>
									<th class='center'>SALDOS</th>
								</tr>
								
								".$capital_table."
								
							</table>
						</td>
					</tr>
				</table>
				<br>
				<br>
				<br>
				
				<table class='bal-gen'>
					<tr>
						<td class='center tables-td-bal bottom-data'>TOTAL ACTIVO: <span ".$ctas_functions->getClassColor($activo).">".$ctas_functions->getNumberFormat($activo)."</span></td>
						<td class='center tables-td-bal bottom-data'>TOTAL PASIVO Y CAPITAL: <span ".$ctas_functions->getClassColor($tot_cap_pas).">".$ctas_functions->getNumberFormat($tot_cap_pas)."</td>
					</tr>
				</table>
		";
		
	$html_end = 
		"
			</div>
		</body>
		";
	$pdf = new \Mpdf\Mpdf();
	$pdf->SetHeader('Sysbit ERP|'.$conf->global->MAIN_INFO_SOCIETE_NOM.'|Página {PAGENO} de {nb}');
	$pdf->SetFooter('Reporte generado el {DATE d-M-Y H:m:s}');
	$pdf->WriteHTML($html_start);
	
	$pdf->WriteHTML($html_table_declaration);

	$pdf->WriteHTML($html_end);
	$pdf->Output();
?>