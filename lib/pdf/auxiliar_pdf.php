<?php

	// Load Dolibarr environment
	$res=0;
	// Try main.inc.php into web root known defined into CONTEXT_DOCUMENT_ROOT (not always defined)
	if (! $res && ! empty($_SERVER["CONTEXT_DOCUMENT_ROOT"])) $res=@include($_SERVER["CONTEXT_DOCUMENT_ROOT"]."/main.inc.php");
	// Try main.inc.php into web root detected using web root caluclated from SCRIPT_FILENAME
	$tmp=empty($_SERVER['SCRIPT_FILENAME'])?'':$_SERVER['SCRIPT_FILENAME'];$tmp2=realpath(__FILE__); $i=strlen($tmp)-1; $j=strlen($tmp2)-1;
	while($i > 0 && $j > 0 && isset($tmp[$i]) && isset($tmp2[$j]) && $tmp[$i]==$tmp2[$j]) { $i--; $j--; }
	if (! $res && $i > 0 && file_exists(substr($tmp, 0, ($i+1))."/main.inc.php")) $res=@include(substr($tmp, 0, ($i+1))."/main.inc.php");
	if (! $res && $i > 0 && file_exists(dirname(substr($tmp, 0, ($i+1)))."/main.inc.php")) $res=@include(dirname(substr($tmp, 0, ($i+1)))."/main.inc.php");
	// Try main.inc.php using relative path
	if (! $res && file_exists("../../main.inc.php")) $res=@include("../../main.inc.php");
	if (! $res && file_exists("../../../main.inc.php")) $res=@include("../../../main.inc.php");
	if (! $res) die("Include of main fails");
	
	include __DIR__ . '/../vendor/autoload.php';
	
	if (file_exists(DOL_DOCUMENT_ROOT . '/reports/class/accounts.class.php')) {
		require_once DOL_DOCUMENT_ROOT . '/reports/class/accounts.class.php';
	}

	global $langs, $user, $conf, $db;
	
	$startdate = GETPOST('startdate');
    $enddate = GETPOST('enddate');
	
	$logo = (isset($_SERVER['HTTPS']) ? "https://" : "http://") . $_SERVER['HTTP_HOST'] . DOL_URL_ROOT .'/documents/mycompany/logos/thumbs/'. $mysoc->logo_mini;
	
	$html_start .= 
		"<!DOCTYPE html>
		<html>
			<head>
				<style>
					html,body{
						font-family:DejaVuSans; 
						font-size:9px;
					}
					#company{
						font-size:10px
					}
					#company .title{
						font-size:12px
					} 
					#invoice
					{
						max-width:800px;
						margin:0 auto;
					}
					#company{
						width:100%;
						border-collapse:collapse;
					}
					#company td{
						padding:3px
					}
					#company img{
						max-width:180px;
						height:auto
					}
					.right{
						text-align:right
					}
					.center{
						text-align:center
					}
					#company td{
						width:25%;
					}
					#company td .tit{
						width:50%;
					}
					.content
					{
						font-size:10px;
						width: 100%;
						border-collapse:collapse;
					}
					.content tr td{
						font-size:10px;
						text-align:center;
						padding: 6px;
					}
					.content td{
						border-bottom:1px solid #0879BF;
					}
					.content th{
						border-bottom:1px solid #0879BF;
						font-weight: bold;
						text-align:center;
						padding: 2px;
					}
					.content .cabecera td{
						font-weight: bold;
						border: none;
						background-color: #f2f2f2;
					}
					.content .cuentas td{
						border: none;
						font-size:10px;
						padding: 1px;
						border-bottom: 0.3px solid #bfbfbf;
					}
					.content .totales td{
						background-color: #f2f2f2;
						font-weight: bold;
						border-bottom: 0.5px solid #bfbfbf;
					}
					.negative-money{
						color: red;
					}
				</style>
			</head>
		<body>
			<div id='invoice'>
				<table id='company'>
					<tr>
						<td rowspan='3'><img src='".$logo."'/></td>
						<td class='center title'><strong>REPORTE AUXILIAR DE CUENTAS</strong></td>
						<td></td>
					</tr>
					<tr>
						<td class='center'>MOVIMIENTOS DEL <strong>$startdate</strong> AL <strong>$enddate</strong></td>
						<td></td>
					</tr>
					<tr>
						<td class='center'>MONEDA: PESO MEXICANO</td>
						<td></td>
					</tr>
				</table>
				
				<br>
		";
	$html_table_declaration = 
		"	
				<table class='content'>
					<tr>
						<th colspan='2'>CUENTA</th>
						<th colspan='2'>NOMBRE</th>
						<th rowspan='2'>REFERENCIA</th>
						<th rowspan='2'>CARGOS</th>
						<th rowspan='2'>ABONOS</th>
						<th>SALDO INICIAL</th>
					</tr>
					<tr>
						<th>FECHA</th>
						<th>TIPO</th>
						<th>NUMERO</th>
						<th>CONCEPTO</th>
						<th>SALDOS</th>
					</tr>
		";
	
	$html_table_end = 
		"
				</table>
		";
		
	$html_end = 
		"
			</div>
		</body>
		";
	$pdf = new \Mpdf\Mpdf();
	$pdf->SetHeader('Sysbit ERP|'.$conf->global->MAIN_INFO_SOCIETE_NOM.'|Página {PAGENO} de {nb}');
	$pdf->SetFooter('Reporte generado el {DATE d-M-Y H:m:s}');
	$pdf->WriteHTML($html_start);
	
	
	$ctas_functions = new Accounts($db);
	$cta = GETPOST('cta');
	$cta2 = GETPOST('cta2');
	
	//Get parent
	$sql = "select subctade from ".MAIN_DB_PREFIX."contab_cat_ctas where rowid = '" . $cta . "' ";
	$resql = $db->query($sql);
	$parent = "";
	while($row = $db->fetch_object($resql)) 
	{
		$parent = " and subctade = " . $row->subctade;
	}
	//Get Accounts names
	$sql = "SELECT cta FROM ".MAIN_DB_PREFIX."contab_cat_ctas where rowid = '" . $cta . "'";
	$resql = $db->query($sql);
	$cta_desc = "";
	while($row = $db->fetch_object($resql)) 
	{
		$cta_desc = $row->cta;
	}
	$sql_accounts = "SELECT rowid FROM ".MAIN_DB_PREFIX."contab_cat_ctas where cta = '" . $cta_desc . "'";
	
	if($cta2 > 0)
	{
		$sql = "SELECT cta FROM ".MAIN_DB_PREFIX."contab_cat_ctas where rowid = '" . $cta2 . "'";
		$resql = $db->query($sql);
		$cta2_desc = "";
		while($row = $db->fetch_object($resql)) 
		{
			$cta2_desc = $row->cta;
		}
		$sql_accounts = "SELECT rowid FROM ".MAIN_DB_PREFIX."contab_cat_ctas where cta between '" . $cta_desc . "' and '" . $cta2_desc . "' $parent ";
	}
	
	//Get parents
	$resql = $db->query($sql_accounts);
	$accounts = array();
	while($row = $db->fetch_object($resql)) 
	{
		array_push($accounts, $row->rowid);
	}
	
	for($i = 0; $i < count($accounts); $i++)
    {
    	if($i > 0)
			$pdf->WriteHTML("<pagebreak />");
		else
			$cont_info ++;
			
    	$sql = "select *, (debe-haber) as saldo from (
	    		select cuenta as rowid, cta, descta, cuenta, sum(debe) as debe, sum(haber) as haber, 'closed' as state from( 
	    		select rowid,cta,descta,subctade 
	    			from (
	    				select * from llx_contab_cat_ctas 
	    				order by subctade, rowid
	    			) products_sorted, 
	    			(select @pv := ".$accounts[$i].") initialisation 
					where (find_in_set(subctade, @pv) > 0 or rowid = ".$accounts[$i].") and @pv := concat(@pv, ',', rowid)) ctas 
	    			inner join llx_contab_polizasdet pdet on pdet.cuenta = ctas.cta 
					INNER JOIN llx_contab_polizas as p on pdet.fk_poliza = p.rowid
					where p.fecha between '".$startdate."' and '".$enddate."'
				group by cta, descta, cuenta ) result ";
				
	    $resql = $db->query($sql);
	    $cont_info = 0;
		while($row = $db->fetch_object($resql)) 
		{
			if($cont_info > 0)
				$pdf->WriteHTML("<pagebreak />");
			else
				$cont_info ++;
				
			$table_content = "";
			//GetSaldoInicial
			$saldo_inicial = $ctas_functions->fetch_initial_mounts($row->rowid, $startdate);
			
			if($saldo_inicial["status"] == 1)
				$saldo_in = $saldo_inicial["saldo"];
			else
				$saldo_in = 0;
				
			$table_content .= 
				"<tr class='cabecera'>
					<td colspan='2'>".$row->cta."</td>
					<td colspan='2'>".$row->descta."</td>
					<td></td>
					<td></td>
					<td></td>
					<td ".$ctas_functions->getClassColor($saldo_in).">".$ctas_functions->getNumberFormat($saldo_in)."</td>
				</tr>";
			
			
			//Detail
			$sql_sub = "select *, (debe - haber) as saldo from(
	        		select rowid, fecha, tipo_pol, cons, descta, sum(debe) as debe, sum(haber) as haber from (
						SELECT p.rowid as rowid, p.fecha, p.tipo_pol, p.cons, p.concepto as descta, det.debe, det.haber FROM llx_contab_polizasdet as det
						INNER JOIN llx_contab_polizas as p on det.fk_poliza = p.rowid
						INNER JOIN llx_contab_cat_ctas as cta on det.cuenta = cta.cta
						where p.fecha between '".$startdate."' and '".$enddate."' and cta = '".$row->rowid."'
					order by fecha ) result group by rowid, fecha, tipo_pol, cons, descta ) result order by fecha";
			$resql_sub = $db->query($sql_sub);
			while($row_sub = $db->fetch_object($resql_sub)) 
			{
				$table_content .= 
					"<tr class='cuentas'>
						<td>".$row_sub->fecha."</td>
						<td>".$row_sub->tipo_pol."</td>
						<td>".$row_sub->cons."</td>
						<td>".$row_sub->descta."</td>
						<td></td>
						<td ".$ctas_functions->getClassColor($row_sub->debe).">".$ctas_functions->getNumberFormat($row_sub->debe)."</td>
						<td ".$ctas_functions->getClassColor($row_sub->haber).">".$ctas_functions->getNumberFormat($row_sub->haber)."</td>
						<td ".$ctas_functions->getClassColor($row_sub->saldo).">".$ctas_functions->getNumberFormat($row_sub->saldo)."</td>
					</tr>";
			}
			
			$total = floatval($row->saldo) + floatval($saldo_inicial["saldo"]);
			$table_content .= 
				"<tr class='totales'>
					<td></td>
					<td></td>
					<td></td>
					<td></td>
					<td>TOTAL:</td>
					<td ".$ctas_functions->getClassColor($row_sub->debe).">".$ctas_functions->getNumberFormat($row->debe)."</td>
					<td ".$ctas_functions->getClassColor($row_sub->haber).">".$ctas_functions->getNumberFormat($row->haber)."</td>
					<td ".$ctas_functions->getClassColor($total).">".$ctas_functions->getNumberFormat($total)."</td>
				</tr>";
			
			$pdf->WriteHTML($html_table_declaration);
			$pdf->WriteHTML($table_content);
			$pdf->WriteHTML($html_table_end);
		}
		
    }
	

	$pdf->WriteHTML($html_end);
	$pdf->Output();
?>