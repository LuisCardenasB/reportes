<?php

	// Load Dolibarr environment
	$res=0;
	// Try main.inc.php into web root known defined into CONTEXT_DOCUMENT_ROOT (not always defined)
	if (! $res && ! empty($_SERVER["CONTEXT_DOCUMENT_ROOT"])) $res=@include($_SERVER["CONTEXT_DOCUMENT_ROOT"]."/main.inc.php");
	// Try main.inc.php into web root detected using web root caluclated from SCRIPT_FILENAME
	$tmp=empty($_SERVER['SCRIPT_FILENAME'])?'':$_SERVER['SCRIPT_FILENAME'];$tmp2=realpath(__FILE__); $i=strlen($tmp)-1; $j=strlen($tmp2)-1;
	while($i > 0 && $j > 0 && isset($tmp[$i]) && isset($tmp2[$j]) && $tmp[$i]==$tmp2[$j]) { $i--; $j--; }
	if (! $res && $i > 0 && file_exists(substr($tmp, 0, ($i+1))."/main.inc.php")) $res=@include(substr($tmp, 0, ($i+1))."/main.inc.php");
	if (! $res && $i > 0 && file_exists(dirname(substr($tmp, 0, ($i+1)))."/main.inc.php")) $res=@include(dirname(substr($tmp, 0, ($i+1)))."/main.inc.php");
	// Try main.inc.php using relative path
	if (! $res && file_exists("../../main.inc.php")) $res=@include("../../main.inc.php");
	if (! $res && file_exists("../../../main.inc.php")) $res=@include("../../../main.inc.php");
	if (! $res) die("Include of main fails");
	
	include __DIR__ . '/../vendor/autoload.php';
	
	if (file_exists(DOL_DOCUMENT_ROOT . '/reports/class/accounts.class.php')) {
		require_once DOL_DOCUMENT_ROOT . '/reports/class/accounts.class.php';
	}

	global $langs, $user, $conf, $db;
	
	$startdate = GETPOST('startdate');
    $enddate = GETPOST('enddate');
	
	$logo = (isset($_SERVER['HTTPS']) ? "https://" : "http://") . $_SERVER['HTTP_HOST'] . DOL_URL_ROOT .'/documents/mycompany/logos/thumbs/'. $mysoc->logo_mini;
	
	$html_start .= 
		"<!DOCTYPE html>
		<html>
			<head>
				<style>
					html,body{
						font-family:DejaVuSans; 
						font-size:9px;
					}
					#company{
						font-size:10px
					}
					#company .title{
						font-size:12px
					} 
					#invoice
					{
						max-width:800px;
						margin:0 auto;
					}
					#company{
						width:100%;
						border-collapse:collapse;
					}
					#company td{
						padding:3px
					}
					#company img{
						max-width:180px;
						height:auto
					}
					.right{
						text-align:right
					}
					.left{
						text-align:left !important;
					}
					.center{
						text-align:center
					}
					#company td{
						width:25%;
					}
					#company td .tit{
						width:50%;
					}
					.content
					{
						font-size:10px;
						width: 100%;
						border-collapse:collapse;
					}
					.content tr td{
						font-size:10px;
						//text-align:center;
						padding: 6px;
					}
					.content td{
						border-bottom:1px solid #0879BF;
					}
					.content th{
						border-bottom:1px solid #0879BF;
						font-weight: bold;
						//text-align:center;
						padding: 6px;
					}
					.content .cabecera td{
						font-weight: bold;
						border: none;
						background-color: #f2f2f2;
					}
					.content .cuentas td{
						border: none;
						font-size:10px;
						padding: 1px;
						border-bottom: 0.3px solid #bfbfbf;
					}
					.content .totales td{
						background-color: #f2f2f2;
						font-weight: bold;
						border-bottom: 0.5px solid #bfbfbf;
					}
					.negative-money{
						color: red;
					}
				</style>
			</head>
		<body>
			<div id='invoice'>
				<table id='company'>
					<tr>
						<td rowspan='3'><img src='".$logo."'/></td>
						<td class='center title'><strong>REPORTE DE ESTADO DE RESULTADOS</strong></td>
						<td></td>
					</tr>
					<tr>
						<td class='center tit'>MOVIMIENTOS DEL <strong>$startdate</strong> AL <strong>$enddate</strong></td>
						<td></td>
					</tr>
					<tr>
						<td class='center'>MONEDA: PESO MEXICANO</td>
						<td></td>
					</tr>
				</table>
				
				<br>
		";
	$html_table_declaration = 
		"	
				<table class='content'>
					<tr>
						<th>CUENTA</th>
						<th class='left' colspan='2'>NOMBRE</th>
						<th>SALDOS</th>
					</tr>
		";
	
	$html_table_end = 
		"
				</table>
		";
		
	$html_end = 
		"
			</div>
		</body>
		";
	$pdf = new \Mpdf\Mpdf();
	$pdf->SetHeader('Sysbit ERP|'.$conf->global->MAIN_INFO_SOCIETE_NOM.'|Página {PAGENO} de {nb}');
	$pdf->SetFooter('Reporte generado el {DATE d-M-Y H:m:s}');
	$pdf->WriteHTML($html_start);
	
	/* LOGICA DEL REPORTE */
	$pdf->WriteHTML($html_table_declaration);
	$ctas_functions = new Accounts($db);
	$ventas = 0;
	$cta_venta = $ctas_functions->fetch_ventas_acount();
	$sql = "SELECT cc.rowid, cc.cta, cc.descta, 0 as saldo, 'closed' as state FROM ".MAIN_DB_PREFIX."contab_cat_ctas as cc
				inner join ".MAIN_DB_PREFIX."contab_sat_ctas sc on cc.fk_sat_cta = sc.rowid
				where cc.subctade = $cta_venta";
	
	$resql = $db->query($sql);
	$data_tmp = array();
	$contenido_html = "";
	while($row = $db->fetch_object($resql)) 
	{
		$value = $ctas_functions->fetch_mounts_of_period($row->rowid, $startdate, $enddate);
		if($value["status"] == 1)
		{
			$row->saldo = strval($value["saldo"]);
			$ventas = $ventas + floatval($value["saldo"]);
			$contenido_html .= 
				"	<tr class='cuentas'>
						<td class='center'>".$row->cta."</td>
						<td class='center'></td>
						<td class='left'>".$row->descta."</td>
						<td class='center' ".$ctas_functions->getClassColor($value["saldo"]).">".$ctas_functions->getNumberFormat($value["saldo"])."</td>
					</tr>
				";
		}
	}
	
	$cuenta_html = 
		"	<tr class='cabecera'>
				<td></td>
				<td class='left' colspan='2'>INGRESOS POR VENTAS</td>
				<td class='center' ".$ctas_functions->getClassColor($ventas).">".$ctas_functions->getNumberFormat($ventas)."</td>
			</tr>
		";
	$pdf->WriteHTML($cuenta_html);
	$pdf->WriteHTML($contenido_html);
	
	
	$costos = 0;
	$cta_costos = $ctas_functions->fetch_costos_acounts();
	$sql = "SELECT cc.rowid, cc.cta, cc.descta, 0 as saldo, 'closed' as state FROM ".MAIN_DB_PREFIX."contab_cat_ctas as cc
			inner join ".MAIN_DB_PREFIX."contab_sat_ctas sc on cc.fk_sat_cta = sc.rowid
			where cc.rowid in (" . implode (", ", $cta_costos) . ")";
	
	$resql = $db->query($sql);
	$data_tmp = array();
	$contenido_html = "";
	while($row = $db->fetch_object($resql)) 
	{
		$value = $ctas_functions->fetch_mounts_of_period($row->rowid, $startdate, $enddate);
		if($value["status"] == 1)
		{
			$row->saldo = strval($value["saldo"]);
			$costos = $costos + floatval($value["saldo"]);
			
			$contenido_html .= 
				"	<tr class='cuentas'>
						<td class='center'>".$row->cta."</td>
						<td class='center'></td>
						<td class='left'>".$row->descta."</td>
						<td class='center' ".$ctas_functions->getClassColor($value["saldo"]).">".$ctas_functions->getNumberFormat($value["saldo"])."</td>
					</tr>
				";
		}
		
	}
	
	$cuenta_html = 
		"	<tr class='cabecera'>
				<td></td>
				<td class='left' colspan='2'>COSTOS DE VENTA</td>
				<td class='center' ".$ctas_functions->getClassColor($costos).">".$ctas_functions->getNumberFormat($costos)."</td>
			</tr>
		";
	$pdf->WriteHTML($cuenta_html);
	$pdf->WriteHTML($contenido_html);
	
	$ut_bruta_calc = $ventas - $costos;
	$text_ut_bruta_calc = "UTILIDAD BRUTA";
	if($ut_bruta_calc < 0)
		$text_ut_bruta_calc = "PERDIDA BRUTA";
	
    $cuenta_html = 
		"	<tr class='cabecera'>
				<td></td>
				<td class='left' colspan='2'>".$text_ut_bruta_calc."</td>
				<td class='center' ".$ctas_functions->getClassColor($ut_bruta_calc).">".$ctas_functions->getNumberFormat($ut_bruta_calc)."</td>
			</tr>
		";
	$pdf->WriteHTML($cuenta_html);
    
    
    $gastos = 0;
	$cta_gastos = $ctas_functions->fetch_gastos_acounts();
	$sql = "SELECT cc.rowid, cc.cta, cc.descta, 0 as saldo, 'closed' as state FROM ".MAIN_DB_PREFIX."contab_cat_ctas as cc
			inner join ".MAIN_DB_PREFIX."contab_sat_ctas sc on cc.fk_sat_cta = sc.rowid
			where cc.rowid in (" . implode (", ", $cta_gastos) . ")";
    
	$resql = $db->query($sql);
	$data_tmp = array();
	$contenido_html = "";
	while($row = $db->fetch_object($resql)) 
	{
		$value = $ctas_functions->fetch_mounts_of_period($row->rowid, $startdate, $enddate);
		if($value["status"] == 1)
		{
			$row->saldo = strval($value["saldo"]);
			$gastos = $gastos + floatval($value["saldo"]);
			
			$contenido_html .= 
				"	<tr class='cuentas'>
						<td class='center'>".$row->cta."</td>
						<td class='center'></td>
						<td class='left'>".$row->descta."</td>
						<td class='center' ".$ctas_functions->getClassColor($value["saldo"]).">".$ctas_functions->getNumberFormat($value["saldo"])."</td>
					</tr>
				";
		}
		
	}
	
	$cuenta_html = 
		"	<tr class='cabecera'>
				<td></td>
				<td class='left' colspan='2'>GASTOS DE OPERACIÓN</td>
				<td class='center' ".$ctas_functions->getClassColor($gastos).">".$ctas_functions->getNumberFormat($gastos)."</td>
			</tr>
		";
	$pdf->WriteHTML($cuenta_html);
	$pdf->WriteHTML($contenido_html);
	
	
	$costos = $costos + $gastos;
	
	$ut_operacion_calc = $ventas - $costos;
	$text_ut_operacion_calc = "UTILIDAD DE OPERACIÓN";
	if($ut_operacion_calc < 0)
		$text_ut_operacion_calc = "PERDIDA DE OPERACIÓN";
			
	$cuenta_html = 
		"	<tr class='cabecera'>
				<td></td>
				<td class='left' colspan='2'>".$text_ut_operacion_calc."</td>
				<td class='center' ".$ctas_functions->getClassColor($ut_operacion_calc).">".$ctas_functions->getNumberFormat($ut_operacion_calc)."</td>
			</tr>
		";
	$pdf->WriteHTML($cuenta_html);
	
	
	$gastos = 0;
	$cta_gastos = $ctas_functions->fetch_gastos_acount();
	$sql = "SELECT cc.rowid, cc.cta, cc.descta, 0 as saldo, 'closed' as state FROM ".MAIN_DB_PREFIX."contab_cat_ctas as cc
			inner join ".MAIN_DB_PREFIX."contab_sat_ctas sc on cc.fk_sat_cta = sc.rowid
			where cc.subctade = $cta_gastos";
	$resql = $db->query($sql);
	$data_tmp = array();
	$contenido_html = "";
	while($row = $db->fetch_object($resql)) 
	{
		$value = $ctas_functions->fetch_mounts_of_period($row->rowid, $startdate, $enddate);
		if($value["status"] == 1)
		{
			$row->saldo = strval($value["saldo"]);
			$gastos = $gastos + floatval($value["saldo"]);
			
			$contenido_html .= 
				"	<tr class='cuentas'>
						<td class='center'>".$row->cta."</td>
						<td class='center'></td>
						<td class='left'>".$row->descta."</td>
						<td class='center' ".$ctas_functions->getClassColor($value["saldo"]).">".$ctas_functions->getNumberFormat($value["saldo"])."</td>
					</tr>
				";
		}
		
	}
	
	$cuenta_html = 
		"	<tr class='cabecera'>
				<td></td>
				<td class='left' colspan='2'>RESULTADO INTEGRAL DE FINANCIAMIENTO</td>
				<td class='center' ".$ctas_functions->getClassColor($gastos).">".$ctas_functions->getNumberFormat($gastos)."</td>
			</tr>
		";
	$pdf->WriteHTML($cuenta_html);
	$pdf->WriteHTML($contenido_html);
	
	if($gastos < 0)
		$gastos = abs($gastos);
		
	$utilidad_antes_impuestos = $ut_operacion_calc - $gastos;
	$text_utilidad_antes_impuestos = "UTILIDAD ANTES DE IMPUESTOS A LA UTILIDAD";
	if($utilidad_antes_impuestos < 0)
		$text_utilidad_antes_impuestos = "PERDIDA ANTES DE IMPUESTOS A LA UTILIDAD";
	
	$cuenta_html = 
		"	<tr class='cabecera'>
				<td></td>
				<td class='left' colspan='2'>".$text_utilidad_antes_impuestos."</td>
				<td class='center' ".$ctas_functions->getClassColor($utilidad_antes_impuestos).">".$ctas_functions->getNumberFormat($utilidad_antes_impuestos)."</td>
			</tr>
		";
	$pdf->WriteHTML($cuenta_html);
	
	//Get all applicable Taxes
	$taxes = $ctas_functions->get_applicable_taxes();
	$utilidad_neta = $utilidad_antes_impuestos;
	for($i = 0; $i < count($taxes); $i++ )
	{
		$utilidad_menos_impuesto = $utilidad_antes_impuestos * $taxes[$i]["value"];
		$utilidad_neta = $utilidad_neta - $utilidad_menos_impuesto;
		$cuenta_html = 
			"	<tr class='cabecera'>
					<td></td>
					<td class='left' colspan='2'>".$taxes[$i]["name"]."</td>
					<td class='center' ".$ctas_functions->getClassColor($utilidad_menos_impuesto).">".$ctas_functions->getNumberFormat($utilidad_menos_impuesto)."</td>
				</tr>
			";
		$pdf->WriteHTML($cuenta_html);
	}
	
	$text_utilidad_neta = "UTILIDAD NETA";
	if($utilidad_neta < 0)
		$text_utilidad_neta = "PERDIDA NETA";
	
	$cuenta_html = 
		"	<tr class='cabecera'>
				<td></td>
				<td class='left' colspan='2'>".$text_utilidad_neta."</td>
				<td class='center' ".$ctas_functions->getClassColor($utilidad_neta).">".$ctas_functions->getNumberFormat($utilidad_neta)."</td>
			</tr>
		";
	$pdf->WriteHTML($cuenta_html);
	
	$pdf->WriteHTML($html_table_end);
	/*FIN DE REPORTE*/

	$pdf->WriteHTML($html_end);
	$pdf->Output();
?>