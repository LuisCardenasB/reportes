<?php
/* Copyright (C) ---Put here your own copyright and developer email---
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/**
 * \file    htdocs/modulebuilder/template/lib/mymodule.lib.php
 * \ingroup mymodule
 * \brief   Library files with common functions for MyModule
 */

/**
 * Prepare admin pages header
 *
 * @return array
 */
function reportsAdminPrepareHead()
{
	global $langs, $conf;

	$langs->load("reports@reports");

	$h = 0;
	$head = array();

	$head[$h][0] = dol_buildpath("/reports/admin/setup.php", 1);
	$head[$h][1] = $langs->trans("Configuración");
	$head[$h][2] = 'settings';
	$h++;
	$head[$h][0] = dol_buildpath("/reports/admin/about.php", 1);
	$head[$h][1] = $langs->trans("Acerca de");
	$head[$h][2] = 'about';
	$h++;

	// Show more tabs from modules
	// Entries must be declared in modules descriptor with line
	//$this->tabs = array(
	//	'entity:+tabname:Title:@mymodule:/mymodule/mypage.php?id=__ID__'
	//); // to add new tab
	//$this->tabs = array(
	//	'entity:-tabname:Title:@mymodule:/mymodule/mypage.php?id=__ID__'
	//); // to remove a tab
	complete_head_from_modules($conf, $langs, $object, $head, $h, 'reports');

	return $head;
}

function reportsUserPrepareHead()
{
	global $langs, $user, $conf, $db;
	
	$langs->load("reports@reports");
	$h = 0;
	$head = array();
	
	$sql = 'SELECT rowid, status, label, description, technical_name
				FROM '.MAIN_DB_PREFIX.'reports_catalog where status = 1 ';
	$resql = $db->query($sql);
	while($res = $db->fetch_object($resql)) 
	{
		$url = "/reports/reports/".$res->technical_name.".php";
		$head[$h][0] = dol_buildpath($url, 1);
		$head[$h][1] = $langs->trans($res->label);
		$head[$h][2] = $res->technical_name;
		$h++;
	}
	

	// Show more tabs from modules
	// Entries must be declared in modules descriptor with line
	//$this->tabs = array(
	//	'entity:+tabname:Title:@mymodule:/mymodule/mypage.php?id=__ID__'
	//); // to add new tab
	//$this->tabs = array(
	//	'entity:-tabname:Title:@mymodule:/mymodule/mypage.php?id=__ID__'
	//); // to remove a tab
	complete_head_from_modules($conf, $langs, $object, $head, $h, 'reports');

	return $head;
}
