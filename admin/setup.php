<?php
/* Copyright (C) 2004-2017 Laurent Destailleur  <eldy@users.sourceforge.net>
 * Copyright (C) ---Put here your own copyright and developer email---
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/**
 * \file    htdocs/modulebuilder/template/admin/setup.php
 * \ingroup mymodule
 * \brief   MyModule setup page.
 */

// Load Dolibarr environment
$res=0;
// Try main.inc.php into web root known defined into CONTEXT_DOCUMENT_ROOT (not always defined)
if (! $res && ! empty($_SERVER["CONTEXT_DOCUMENT_ROOT"])) $res=@include($_SERVER["CONTEXT_DOCUMENT_ROOT"]."/main.inc.php");
// Try main.inc.php into web root detected using web root caluclated from SCRIPT_FILENAME
$tmp=empty($_SERVER['SCRIPT_FILENAME'])?'':$_SERVER['SCRIPT_FILENAME'];$tmp2=realpath(__FILE__); $i=strlen($tmp)-1; $j=strlen($tmp2)-1;
while($i > 0 && $j > 0 && isset($tmp[$i]) && isset($tmp2[$j]) && $tmp[$i]==$tmp2[$j]) { $i--; $j--; }
if (! $res && $i > 0 && file_exists(substr($tmp, 0, ($i+1))."/main.inc.php")) $res=@include(substr($tmp, 0, ($i+1))."/main.inc.php");
if (! $res && $i > 0 && file_exists(dirname(substr($tmp, 0, ($i+1)))."/main.inc.php")) $res=@include(dirname(substr($tmp, 0, ($i+1)))."/main.inc.php");
// Try main.inc.php using relative path
if (! $res && file_exists("../../main.inc.php")) $res=@include("../../main.inc.php");
if (! $res && file_exists("../../../main.inc.php")) $res=@include("../../../main.inc.php");
if (! $res) die("Include of main fails");

global $langs, $user, $conf, $db;

// Libraries
require_once DOL_DOCUMENT_ROOT . "/core/lib/admin.lib.php";
require_once '../lib/reports.lib.php';
//require_once "../class/myclass.class.php";

// Translations
$langs->loadLangs(array("admin", "mymodule@reports"));

// Access control
if (! $user->admin) accessforbidden();

// Parameters
$action = GETPOST('action', 'alpha');
$backtopage = GETPOST('backtopage', 'alpha');


$arrayofparameters = array();

$sql = 'SELECT rowid, status, label, description
			FROM '.MAIN_DB_PREFIX.'reports_catalog ';
$resql = $db->query($sql);

while($res = $db->fetch_object($resql)) {
	array_push( $arrayofparameters, $res );
}


/*
 * Actions
 */

include DOL_DOCUMENT_ROOT.'/core/actions_setmoduleoptions.inc.php';


/*
 * View
 */

$page_name = "Configuración del módulo de reportes contables.";
llxHeader('', $langs->trans($page_name));

// Subheader
$linkback = '<a href="'.($backtopage?$backtopage:DOL_URL_ROOT.'/admin/modules.php?restore_lastsearch_values=1').'">'.$langs->trans("BackToModuleList").'</a>';

print load_fiche_titre($langs->trans($page_name), $linkback, 'object_mymodule@reports');

// Configuration header
$head = reportsAdminPrepareHead();
dol_fiche_head($head, 'settings', '', -1, "reports@reports");

// Setup page goes here
echo $langs->trans("Reportes disponibles");


if ($action == 'edit')
{
	print '<form method="POST" action="'.$_SERVER["PHP_SELF"].'">';
	print '<input type="hidden" name="token" value="'.$_SESSION['newtoken'].'">';
	//print '<input type="hidden" name="action" value="update">';

	print '
		<table class="noborder" width="100%">
			<tr class="liste_titre">
				<td class="titlefield">Habilitar</td>
				<td>Nombre del reporte</td>
				<td>Descripción</td>
			</tr>';
	foreach($arrayofparameters as $val)
	{
		$checked = $val->status ? "checked" : "";
		print '	<tr class="oddeven">
				<td>
					<input type="checkbox" name="reports[]" value="'.$val->rowid.'" '.$checked.' class="flat minwidth100" />
				</td>
				<td>
					'.$val->label.'
				</td>
				<td>
					'.$val->description.'
				</td>
			</tr>';
	}
	print '	
		</table>
	';
	
	print '<br><div class="center">';
	print '<input class="button" type="submit" value="'.$langs->trans("Save").'">';
	print '</div>';

	print '</form>';
	print '<br>';
}
else
{
	print '
		<table class="noborder" width="100%">
			<tr class="liste_titre">
				<td class="titlefield">Habilitar</td>
				<td>Nombre del reporte</td>
				<td>Descripción</td>
			</tr>';
	foreach($arrayofparameters as $val)
	{ 
			$selected_reports = GETPOST('reports', 'alpha');
			if(isset($selected_reports) && $selected_reports != "")
			{
				$was_finded = 0;
				//Update database
				foreach($selected_reports as $report)
				{
					if($report == $val->rowid)
					{
						$db->begin();   // Start transaction
						$db->query("update ".MAIN_DB_PREFIX."reports_catalog set status = 1 where rowid = ".$val->rowid);
						$db->commit();       // Validate transaction
						//or $db->rollback()  // Cancel transaction
						$val->status = 1;
						$was_finded = 1;
						break;
					}
				}
				//Update variables.
				if($was_finded == 0)
				{
					$db->begin();   // Start transaction
					$db->query("update ".MAIN_DB_PREFIX."reports_catalog set status = 0 where rowid = ".$val->rowid);
					$db->commit();       // Validate transaction
					//or $db->rollback()  // Cancel transaction
					$val->status = 0;
				}
			}
		
		$status = $val->status ? 'Habilitado' : 'Deshabilitado';
		print '	<tr class="oddeven">
				<td>
					'. $status .'
				</td>
				<td>
					'.$val->label.'
				</td>
				<td>
					'.$val->description.'
				</td>
			</tr>';
	}
	print '	
		</table>
	';

	print '<div class="tabsAction">';
	print '<a class="butAction" href="'.$_SERVER["PHP_SELF"].'?action=edit">'.$langs->trans("Modify").'</a>';
	print '</div>';
}


// Page end
dol_fiche_end();

llxFooter();
$db->close();
